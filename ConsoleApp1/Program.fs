﻿// // For more information see https://aka.ms/fsharp-console-apps
// // printfn "Hello from F#"

open FSharp.Data.HttpStatusCodes
open LiteDB

open FSharp.Interop.Excel
open Microsoft.FSharp.Core

open 型钢



// [<CLIMutable>]
// type Customer =
//     {
//
//       Id: int
//       Name: string
//       Phones: string[]
//       IsActive: bool }

let db = new LiteDatabase(@"C:\Users\vic\Desktop\MyData.db")
// let col = db.GetCollection<Customer>("customers")
//
// let customer =
//     {
//
//       Id = 0
//       Name = "John Doe"
//       Phones = [| "867-5309" |]
//       IsActive = true }
//
// let customer1 =
//     { Id = 0 // 多个数据的时候id都设置为 一样， 不然提示id重复
//       Name = "John king"
//       Phones = [| "867-5334" |]
//       IsActive = false }
//
// col.Insert(customer) |> ignore
// col.Insert(customer1) |> ignore

// or
// col.InsertBulk([customer; customer1]) |> ignore


// let reader = db.Execute("SELECT $ FROM customers")
//
// let mapper = BsonMapper()
//
// let customers =
//     [| while reader.Read() do
//            mapper.Deserialize<Customer>(reader.Current) |]
//
// customers |> Seq.iter (printfn "%A")




let col1 = db.GetCollection<工字钢>("热轧工字钢")
type Steel = ExcelFile< @"C:\Users\vic\Desktop\热轧型钢参数.xlsx","热轧工字钢">
let steelFile = new Steel()
let rows = steelFile.Data |> Seq.toArray
col1.InsertBulk(
    seq {
        for row in rows do
            yield {
            Id = 0
            型号 = row.型号
            腿宽b = int row.b
            高度h = int row.h
            腰厚d =  row.d 
            平均腿厚t =  row.t 
            内圆弧半径r =  row.r 
            腿端圆弧半径r1 =  row.r1 
            截面面积 = row.截面面积 
            理论重量 =  row.理论重量 
            标准 = "GB/T 706-2016"
            外表面积 = row.外表面积
            惯性矩Ix = row.Ix
            惯性矩Iy = row.Iy
            惯性半径ix = row.ix
            惯性半径iy = row.iy
            截面模数Wx = row.Wx
            截面模数Wy = row.Wy
            }
    }) |> ignore

type SteelCao = ExcelFile< @"C:\Users\vic\Desktop\热轧型钢参数.xlsx","热轧槽钢">
let steelcaofile = new SteelCao()
let caorows = steelcaofile.Data |> Seq.toArray

let col2 = db.GetCollection<槽钢>("热轧槽钢")
col2.InsertBulk(
    seq{
        for row in caorows do
            yield {
                Id = 0
                型号 = row.型号
                高度h = int row.h
                腿宽b = int row.b
                腰厚d = row.d
                平均腿厚t = row.t
                内圆弧半径r = row.r
                腿端圆弧半径r1 = row.r1
                截面面积 = row.截面面积
                理论重量 = row.理论重量
                标准 = "GB/T 706-2016"
                外表面积 = row.外表面积
                惯性矩Ix = row.Ix
                惯性矩Iy = row.Iy
                惯性矩Iy1 = row.Iy1
                惯性半径ix = row.ix
                惯性半径iy = row.iy
                截面模数Wx = row.Wx
                截面模数Wy = row.Wy
                重心距离Z0 = row.Z0
                
            }
    }) |> ignore



type SteelDeng = ExcelFile< @"C:\Users\vic\Desktop\热轧型钢参数.xlsx","热轧等边角钢">
let steeldengfile = new SteelDeng()
let dengrows = steeldengfile.Data |> Seq.toArray

let col3 = db.GetCollection<等边角钢>("热轧等边角钢")
col3.InsertBulk(
    seq{
        for row in dengrows do
            yield {
               
                Id = 0
                型号 = row.型号
                边宽b = int row.b
                边厚d =int row.d
                内圆弧半径r = row.r
                截面面积 = row.截面面积
                理论重量 = row.理论重量
                外表面积 = row.外表面积
                标准 = "GB/T 706-2016"
                惯性矩Ix = row.Ix
                惯性矩Ix1 = row.Ix1
                惯性矩Ix0 = row.Ix0
                惯性矩Iy0 = row.Iy0
                
                惯性半径ix = row.ix
                惯性半径ix0 = row.ix0
                惯性半径iy0 = row.iy0
                
                截面模数Wx = row.Wx
                截面模数Wx0 = row.Wx0
                截面模数Wy0 = row.Wy0
                
                重心距离Z0 = row.Z0
                
            }
    }) |> ignore

type SteelbDeng = ExcelFile< @"C:\Users\vic\Desktop\热轧型钢参数.xlsx","热轧不等边角钢">
let steelbdengfile = new SteelbDeng()
let bdengrows = steelbdengfile.Data |> Seq.toArray

let col4 = db.GetCollection<不等边角钢>("热轧不等边角钢")
col4.InsertBulk(
    seq{
        for row in bdengrows do
            yield {
               
                Id = 0
                型号 = row.型号
                
                边厚d =int row.d
                内圆弧半径r = row.r
                截面面积 = row.截面面积
                理论重量 = row.理论重量
                外表面积 = row.外表面积
                标准 = "GB/T 706-2016"
                长边宽度B = int row.B
                短边宽度b = int row.b
                惯性矩Ix = row.Ix
                惯性矩Ix1 = row.Ix1

                惯性半径ix = row.ix

                截面模数Wx = row.Wx
                惯性矩Iy = row.Iy
                惯性矩Iy1 = row.Iy1
                惯性矩Iu = row.Iu
                惯性半径iy = row.iy
                惯性半径iu = row.iu
                截面模数Wy = row.Wy
                截面模数Wu = row.Wu
                重心距离X0 = row.X0
                重心距离Y0 = row.Y0
                
            }
    }) |> ignore


type SteelbLeng = ExcelFile< @"C:\Users\vic\Desktop\方形空心型钢参数.xlsx","Sheet1">
let steellengfile = new SteelbLeng()
let lengrows = steellengfile.Data |> Seq.toArray

let col5 = db.GetCollection<冷弯方形钢管>("冷弯方形钢管")
col5.InsertBulk(
    seq{
        for row in lengrows do
            yield {
               
                Id = 0

                截面面积 = row.截面面积
                理论重量 = row.理论重量

                标准 = "GB/T 6728-2017"
                边长 = int row.边长
                壁厚 = row.壁厚
                惯性矩 = row.惯性矩
                惯性半径 = row.惯性半径
                截面模数 = row.截面模数

                
            }
    }) |> ignore





let collour = db.GetCollection<六角螺栓重量>("六角螺栓重量")
type Musheet = ExcelFile<"C:\\Users\\vic\\Desktop\\1型六角螺母c级.xlsx","螺母重量">

let file = new Musheet()
// let rows = file.Data |> Seq.toArray
// for row in rows do
//     collour.Insert(
//         {
//             Id = 0
//             Name = row.螺栓型号
//             Weight = row.千件重
//         }) |> ignore

type Lour = ExcelFile<"C:\\Users\\vic\\Desktop\\1型六角螺母c级.xlsx", "螺栓重量">
let file1 = new Lour()
let rowss = file1.Data |> Seq.toArray

let ww =
    [| "10"
       "12"
       "16"
       "20"
       "25"
       "30"
       "35"
       "40"
       "45"
       "50"
       "55"
       "60"
       "65"
       "70"
       "80"
       "90"
       "100"
       "110"
       "120"
       "130"
       "140"
       "150"
       "160"
       "180"
       "200"
       "220"
       "240"
       "260"
       "280"
       "300"
       "320"
       "340"
       "360"
       "380"
       "400"
       "420"
       "440"
       "460"
       "480"
       "500" |]
// for row in rowss do
//     for i in ww do
//
//         collour.Insert(
//             {
//                 型号 = row.螺栓型号
//                 长度 = int(i)
//                 重量 =  unbox<float> (row.GetValue i)
//
//             }) |> ignore
collour.InsertBulk(
    seq {
        for row in rowss do
            for i in ww do
                let w = unbox<float> (row.GetValue i)
                if  w <> 0 then
                    yield
                        { Id = 0
                          型号 = row.螺栓型号
                          长度 = int i
                          重量 = w
                        }
    }

)
|> ignore

// for m in collour.Query().Where(fun x -> x.Id < 100).ToArray() do
//     
//     printfn $"Id: {m.Id},型号：{m.型号},长度：{m.长度},重量：{m.重量}"

// let one = collour.FindOne(fun n -> n.型号 = "M20" && n.长度 = 50)
// printfn $"{one}"

type Lomu = ExcelFile<"C:\\Users\\vic\\Desktop\\1型六角螺母c级.xlsx", "六角螺母">
let filelomu = new Lomu()
let filelomudata = filelomu.Data |> Seq.toArray
let lomu = db.GetCollection<六角螺母>("六角螺母")
lomu.InsertBulk(
    seq {
        for row in filelomudata do
            yield {
                Id = 0
                型号 = row.螺母型号
                直径d = int row.d
                螺距P = row.P
                对角宽度e = row.e
                最大高度mmax = row.mmax
                最小高度mmin = row.mmin
                扳拧高度mw = row.mw
                对边宽度s = int row.s
                最小对边宽度smin = row.smin
                重量 = row.w
                标准 = "GB/T 41-2016"
            } 
    }

)
|> ignore

type Lous = ExcelFile<"C:\\Users\\vic\\Desktop\\1型六角螺母c级.xlsx", "六角螺栓">
let filelour = new Lous()
let filelourdata = filelour.Data |> Seq.toArray
let lour = db.GetCollection<六角螺栓>("六角螺栓")
lour.InsertBulk(
    seq {
        for row in filelourdata do
            yield {
                Id = 0
                型号 = row.螺栓型号
                直径d = int row.d
                螺距P = row.P
                最大螺纹到底间距amax = row.amax
                对角宽度e = row.e
                扳拧高度kw = row.kw
                对边宽度s = int row.s
                最小对边宽度smin = row.smin
                标准 = "GB/T 5781-2016"
                最小螺纹到底间距amin = row.amin
                垫圈高度c = row.c
                支撑面内径da = row.da
                垫圈面直径dw = row.dw
                头部高度k = row.k
                最大头部高度kmax = row.kmax
                最小头部高度kmin = row.kmin
                头下圆角半径r = row.r
            } 
    }

)
|> ignore


type Pingdian = ExcelFile<"C:\\Users\\vic\\Desktop\\1型六角螺母c级.xlsx", "平垫圈c级">
let filep = new Pingdian()
let filepdata = filep.Data |> Seq.toArray
let pd = db.GetCollection<平垫圈>("平垫圈")
pd.InsertBulk(
    seq {
        for row in filepdata do
            yield {
                Id = 0
                型号 = row.规格
                公称内径d1 = row.d公称
                最大内径d1max = row.d最大值
                公称外径d2 = row.dc公称
                最小外径d2min = row.dc最小值
                公称厚度h = row.h公称
                最大厚度hmax = row.h最大值
                最小厚度hmin = row.h最小值

                重量 = row.千件重
                标准 = "GB/T 95-2002"
            } 
    }

)
|> ignore

type Pingdiang = ExcelFile<"C:\\Users\\vic\\Desktop\\1型六角螺母c级.xlsx", "工字钢用方斜垫圈">
let filepg = new Pingdiang()
let filepgdata = filepg.Data |> Seq.toArray
let pdg = db.GetCollection<工字钢用方斜垫圈>("工字钢用方斜垫圈")
pdg.InsertBulk(
    seq {
        for row in filepgdata do
            yield {
                Id = 0
                公称内径d = row.d
                重量 = row.w
                标准 = "GB/T 852-1988"
                规格 = row.规格
                宽度B = row.B
                顶边厚度H = row.H
                底边厚度H1 = row.H1
            } 
    }

)
|> ignore


type Pingdianc = ExcelFile<"C:\\Users\\vic\\Desktop\\1型六角螺母c级.xlsx", "槽钢用方斜垫圈">
let filepc = new Pingdianc()
let filepcdata = filepc.Data |> Seq.toArray
let pdc = db.GetCollection<槽钢用方斜垫圈>("槽钢用方斜垫圈")
pdc.InsertBulk(
    seq {
        for row in filepcdata do
            yield {
                Id = 0
                公称内径d = row.d
                重量 = row.w
                标准 = "GB/T 853-1988"
                规格 = row.规格
                宽度B = row.B
                顶边厚度H = row.H
                底边厚度H1 = row.H1
            } 
    }

)
|> ignore





// let teststr (slopeheight,topheight,slopecount,
//              slopedata,rockstratacount, rockstratadata,
//              controlcount,controldata) =
//     let str =
//         $"""
// ##ROCKSLOPE_SP_GJ_V10
// %0.6f{slopeheight},%0.6f{topheight},80.000000
// 0.100000,0.250000,1.000000,0,0,0.008000
// 1,1.300000,10.000000,2
// 1.000000,1.000000,0.690000,0.600000,0,310.000000,2.000000,1.800000,1.500000
// %A{slopecount}
// {slopedata}
// {rockstratacount}
// {rockstratadata}
// {controlcount}
// {controldata}
// 0,2
// 0,0,1.000000,0
// 30.000000,48.599998,30.000000
// 0,8.800000
// 1,7.600000
// 0.000000,0.000000
// 0.250000,1.000000,25.600000,0,30.000000,30.500000,1.500000
// 100.000000,3.000000,90.000000
// ##Name 简单平面滑动稳定分析 6
// ##ChaXun 计算简图 A22_ROCKSLOPE_SP_JT.DXF A22_ROCKSLOPE_JT.RTF 
// ##ChaXun 计算结果 A22_ROCKSLOPE_SP_JG.DXF A22_ROCKSLOPE_SP_JT.RTF 
// ##混凝土钢筋计算标识
// 1
//         """
//     str
//
//
// // let sdata =
// //     """10.000000,10.000000
// // 10.000000,0.000000
// // 10.000000,10.000000
// // 10.000000,0.000000
// // 10.000000,20.000000"""
//
// let sdata =
//     seq{
//     "10.000000,10.000000"
//     "10.000000,0.000000"
//     "10.000000,10.000000"
//     "10.000000,0.000000"
//     "10.000000,20.000000"
//     } |> String.concat "\n"
//
//
//
// let rdata =
//     seq{
//         "21.299999,18.000000,80.000000"
//         "15.000000,16.799999,100.000000"
//         "7.800000,17.000000,50.000000"
//         "-3.200000,20.000000,60.000000"
//         "-6.200000,21.000000,40.000000"
//     } |> String.concat "\n"
// let cdata =
//     seq{
//         "-10.000000"
//         "1.000000"
//         "1.000000"
//         "1.000000"
//         "3.500000"
//         "0.000000"
//         "0.100000"
//         "1.000000"
//         "1.000000"
//         "1.000000"
//         "3.200000"
//         "0.000000"
//     } |> String.concat "\n"
//
//
//
// printfn $"{teststr(40.,0.,5,sdata,5,rdata,2,cdata)}"