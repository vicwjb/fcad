﻿namespace 型钢

[<CLIMutable>]
type 六角螺母 =
    { Id: int
      型号: string
      直径d: int
      螺距P: float
      对角宽度e: float
      最大高度mmax: float
      最小高度mmin: float
      扳拧高度mw: float
      对边宽度s: int
      最小对边宽度smin: float
      重量: float
      标准: string }

[<CLIMutable>]
type 六角螺栓 =
    { Id: int
      型号: string
      直径d: int
      螺距P: float
      最大螺纹到底间距amax: float
      最小螺纹到底间距amin: float
      垫圈高度c: float
      支撑面内径da: float
      垫圈面直径dw: float
      对角宽度e: float
      头部高度k: float
      最大头部高度kmax: float
      最小头部高度kmin: float
      扳拧高度kw: float
      头下圆角半径r: float
      对边宽度s: int
      最小对边宽度smin: float
      标准: string }
    
[<CLIMutable>]
type 六角螺栓重量 = { Id: int; 型号: string; 长度: int; 重量: float }

[<CLIMutable>]
type 平垫圈 =
    { Id: int
      型号: string
      公称内径d1: float
      最大内径d1max: float
      公称外径d2: float
      最小外径d2min: float
      公称厚度h: float
      最大厚度hmax: float
      最小厚度hmin: float
      重量: float
      标准: string }


[<CLIMutable>]
type 工字钢用方斜垫圈 =
    { Id: int
      规格: string
      公称内径d: float
      宽度B: float
      顶边厚度H: float
      底边厚度H1: float
      重量: float
      标准: string }

[<CLIMutable>]
type 槽钢用方斜垫圈 =
    { Id: int
      规格: string
      公称内径d: float
      宽度B: float
      顶边厚度H: float
      底边厚度H1: float
      重量: float
      标准: string }




[<CLIMutable>]
type 槽钢 =
    { Id: int
      型号: string
      高度h: int
      腿宽b: int
      腰厚d: float
      平均腿厚t: float
      内圆弧半径r: float
      腿端圆弧半径r1: float
      截面面积: float
      理论重量: float
      标准: string
      外表面积: float
      惯性矩Ix: float
      惯性矩Iy: float
      惯性矩Iy1: float
      惯性半径ix: float
      惯性半径iy: float
      截面模数Wx: float
      截面模数Wy: float
      重心距离Z0: float }

[<CLIMutable>]
type 工字钢 =
    { Id: int
      型号: string
      高度h: int
      腿宽b: int
      腰厚d: float
      平均腿厚t: float
      内圆弧半径r: float
      腿端圆弧半径r1: float
      截面面积: float
      理论重量: float
      标准: string
      外表面积: float
      惯性矩Ix: float
      惯性矩Iy: float
      惯性半径ix: float
      惯性半径iy: float
      截面模数Wx: float
      截面模数Wy: float }

[<CLIMutable>]
type 等边角钢 =
    { Id: int
      型号: string
      边宽b: int
      边厚d: int
      内圆弧半径r: float
      截面面积: float
      理论重量: float
      外表面积: float
      标准: string
      惯性矩Ix: float
      惯性矩Ix1: float
      惯性矩Ix0: float
      惯性矩Iy0: float
      惯性半径ix: float
      惯性半径ix0: float
      惯性半径iy0: float
      截面模数Wx: float
      截面模数Wx0: float
      截面模数Wy0: float
      重心距离Z0: float }

[<CLIMutable>]
type 不等边角钢 =
    { Id: int
      型号: string
      长边宽度B: int
      短边宽度b: int
      边厚d: int
      内圆弧半径r: float
      截面面积: float
      理论重量: float
      外表面积: float
      标准: string
      惯性矩Ix: float
      惯性矩Ix1: float
      惯性矩Iy: float
      惯性矩Iy1: float
      惯性矩Iu: float
      惯性半径ix: float
      惯性半径iy: float
      惯性半径iu: float
      截面模数Wx: float
      截面模数Wy: float
      截面模数Wu: float
      重心距离X0: float
      重心距离Y0: float }

[<CLIMutable>]
type 冷弯方形钢管 =
    {
        Id: int
        边长: int
        壁厚: float
        理论重量: float
        截面面积: float
        惯性矩: float
        惯性半径: float
        截面模数: float
        标准: string

    }