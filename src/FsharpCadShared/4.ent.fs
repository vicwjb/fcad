namespace FsharpCad

#if acad
open Autodesk.AutoCAD.DatabaseServices
open Autodesk.AutoCAD.Geometry
#endif
#if zcad
open ZwSoft.ZwCAD.DatabaseServices
open ZwSoft.ZwCAD.Geometry
#endif


open Microsoft.FSharp.Collections
open Microsoft.FSharp.Core



/// hahah 

[<AutoOpen>]
module Ent =
    type Point3d with
        member x.Point2d = Point2d(x.X, x.Y)
        member x.Z0 = Point3d(x.X, x.Y, 0)

    let createPline close pts =
        let pl = new Polyline()
        pl.SetDatabaseDefaults()

        pts
        |> Seq.iteri (fun i item ->
            let (pt: Point3d, bulge, startw, endw) = item
            pl.AddVertexAt(i, pt.Point2d, bulge, startw, endw))

        pl.Closed <- close
        pl
