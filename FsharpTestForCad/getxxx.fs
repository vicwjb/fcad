﻿module FsharpTestForCad.getxxx

open System
open FsharpCad

open Autodesk.AutoCAD.ApplicationServices.Core
open Autodesk.AutoCAD.DatabaseServices
open Autodesk.AutoCAD.EditorInput
open Autodesk.AutoCAD.Geometry
open Autodesk.AutoCAD.Runtime
open FsharpCad.Library
open IFoxCAD.Cad
open Microsoft.FSharp.Control
open shigongtu
open System.Collections.Generic

open Autodesk.AutoCAD.ApplicationServices

[<CommandMethod("testifox")>]
let testifox () =
    use tr = new DBTrans()
    let inputs =
        prompt {

            let center = getPointMsg ("\nCenter: ")
            let radius = getDist (center.Value, "\nRadius: ")
            return (center.Value, radius.Value)
        }
    match inputs with
    | None -> ()
    | Some(center, radius) ->
        seq {
            new Circle(center, Vector3d.ZAxis, radius)
            new Circle(center, Vector3d.ZAxis, radius + 500.)
            new Circle(center, Vector3d.ZAxis, radius + 1000.)
        }
        |> addEntities tr.CurrentSpace
        |> ignore
        
    let pts =
        [ Point3d(0, 0, 0)
          Point3d(0, 1, 0)
          Point3d(1, 1, 0)
          Point3d(1, 0, 0) ]
        |> List<Point3d>
    // 如果要和c#的元组互操作，需要使用结构元组
    // struct (a * b)
    // ValueTuple<a,b>
    // 上面两种类型是等效的
    let a: struct (Point3d * double * double * double) seq =
        seq {
            (Point3d(0, 0, 0), 0, 0, 0)
            (Point3d(0, 1, 0), 0, 0, 0)
            (Point3d(1, 1, 0), 0, 0, 0)
            (Point3d(1, 0, 0), 0, 0, 0)
        }
    
    a.CreatePolyline() |> ignore
    let pline = pts.CreatePolyline()
    tr.CurrentSpace.AddEntity(pline) |> ignore

    
    // 可以通过委托，一次性地创建多段线并设置属性
    let pline1 = pts.CreatePolyline (fun c ->
        c.Closed <- true
        c.ConstantWidth <- 0.2
        c.ColorIndex <- 1)

    
    tr.CurrentSpace.AddEntity(pline1) |> ignore

[<CommandMethod("testcegetpoint")>]
let testcegetpoint () =
    use tr = new DBTrans()

    let inputs =
        prompt {

            let center = getPointMsg ("\nCenter: ")
            let radius = getDist (center.Value, "\nRadius: ")
            return (center.Value, radius.Value)
        }

    match inputs with
    | None -> ()
    | Some(center, radius) ->
        seq {
            new Circle(center, Vector3d.ZAxis, radius)
            new Circle(center, Vector3d.ZAxis, radius + 500.)
            new Circle(center, Vector3d.ZAxis, radius + 1000.)
        }
        |> addEntities tr.CurrentSpace
        |> ignore

    
[<CommandMethod("testgongzighang")>]
let testgongzighang () =
    use tr = new DBTrans()

    let p =
        prompt {
            let pt = getPointMsg ("\npick the point:")
            return pt.Value
        }

    let h = 120.0
    let b = 74.0
    let d = 5.0
    let t = 8.4
    let r = 7.0
    let r1 = 3.5

    let h0 = 50.
    let b0 = 37.
    let d0 = 4.5
    let t0 = 7.0



    match p with
    | None -> ()
    | Some p0 ->
        gongzigang p0 h b d t r r1
        |> createPline true
        |> addEntity tr.CurrentSpace
        |> ignore

        caogang p0 h0 b0 d0 t0 r r1
        |> createPline true
        |> addEntity tr.CurrentSpace
        |> ignore

        jiaogang p0 20. 3. 3.5 |> createPline true |> addEntity tr.CurrentSpace |> ignore


        bujiaogang p0 25. 16. 3. 3.5
        |> createPline true
        |> addEntity tr.CurrentSpace
        |> ignore

let appsysvarchanged =
   fun obj (args:SystemVariableChangedEventArgs) ->
        let oval = Application.GetSystemVariable(args.Name)
        Application.ShowAlertDialog(args.Name)
let apphander = SystemVariableChangedEventHandler(
    fun obj e ->
        
        Application.ShowAlertDialog(e.Name))
let appchanged =
   fun (args:SystemVariableChangedEventArgs) ->
        let oval = Application.GetSystemVariable(args.Name)
        Application.ShowAlertDialog(args.Name)
// let inline (+=) (i:IEvent<'T, #EventArgs>) (func :  #EventArgs -> unit) =
//     i |> Event.add func




[<CommandMethod("addappevent")>]
let addappevent() =
    // Application.SystemVariableChanged.AddHandler(appsysvarchanged)
    // Application.SystemVariableChanged
    // |> Event.add (fun args ->
    //     let a = Application.GetSystemVariable(args.Name)
    //     Application.ShowAlertDialog(args.Name))
    Application.SystemVariableChanged -= apphander
    Application.SystemVariableChanged += apphander
    // Application.SystemVariableChanged.AddHandler apphander
[<CommandMethod("removeappevent")>]
let removeappevent () =
    // Application.SystemVariableChanged.RemoveHandler(apphander)
    Application.SystemVariableChanged -= apphander