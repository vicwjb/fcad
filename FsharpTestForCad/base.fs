module shigongtu



open Autodesk.AutoCAD.DatabaseServices
open Autodesk.AutoCAD.Geometry


let dianquan_fushi p0 d1 d2 h =
    let cir = new Circle(p0, Vector3d.ZAxis, d1)
    let cir2 = new Circle(p0, Vector3d.ZAxis, d2)

    seq {
        cir
        cir2
    }




/// <summary>不等边角钢</summary>
/// <param name="p0">基点</param>
/// <param name="B">长边宽度</param>
/// <param name="b">短边宽度</param>
/// <param name="d">边厚</param>
/// <param name="r">内圆弧半径</param>
/// <returns></returns>
let bujiaogang (p0: Point3d) B b d r =

    let r1 = d / 3.0

    let p1 = Point3d(p0.X + b, p0.Y, 0)
    let p2 = Point3d(p1.X, p1.Y + d - r1, 0.)
    let p3 = Point3d(p2.X - r1, p2.Y + r1, 0.)
    let p4 = Point3d(p0.X + d + r, p0.Y + d, 0.)
    let p5 = Point3d(p0.X + d, p0.Y + d + r, 0.)
    let p6 = Point3d(p0.X + d, p0.Y + B - r1, 0.)
    let p7 = Point3d(p0.X + d - r1, p0.Y + B, 0.)
    let p8 = Point3d(p0.X, p0.Y + B, 0.)

    seq {
        (p0, 0., 0., 0.)
        (p1, 0., 0., 0.)
        (p2, 0.414214, 0, 0)
        (p3, 0, 0, 0)
        (p4, -0.414214, 0, 0)
        (p5, 0, 0, 0)
        (p6, 0.414214, 0, 0)
        (p7, 0, 0, 0)
        (p8, 0, 0, 0)
    }

/// <summary>角钢</summary>
/// <param name="p0">基点</param>
/// <param name="b">边宽</param>
/// <param name="d">边厚</param>
/// <param name="r">内圆弧半径</param>
/// <returns></returns>
let jiaogang (p0: Point3d) b d r =

    let r1 = d / 3.0

    let p1 = Point3d(p0.X + b, p0.Y, 0)
    let p2 = Point3d(p1.X, p1.Y + d - r1, 0.)
    let p3 = Point3d(p2.X - r1, p2.Y + r1, 0.)
    let p4 = Point3d(p0.X + d + r, p0.Y + d, 0.)
    let p5 = Point3d(p0.X + d, p0.Y + d + r, 0.)
    let p6 = Point3d(p0.X + d, p0.Y + b - r1, 0.)
    let p7 = Point3d(p0.X + d - r1, p0.Y + b, 0.)
    let p8 = Point3d(p0.X, p0.Y + b, 0.)

    seq {
        (p0, 0., 0., 0.)
        (p1, 0., 0., 0.)
        (p2, 0.414214, 0, 0)
        (p3, 0, 0, 0)
        (p4, -0.414214, 0, 0)
        (p5, 0, 0, 0)
        (p6, 0.414214, 0, 0)
        (p7, 0, 0, 0)
        (p8, 0, 0, 0)
    }


/// <summary>槽钢</summary>
/// <param name="p0">基点</param>
/// <param name="h">高度</param>
/// <param name="b">腿宽</param>
/// <param name="d">腰厚</param>
/// <param name="t">平均腰厚</param>
/// <param name="r">内圆弧半径</param>
/// <param name="r1">腿端圆弧半径</param>
/// <returns></returns>
let caogang (p0: Point3d) h b d t r r1 =
    let ang = atan 10.

    let p1 = Point3d(p0.X + b, p0.Y, 0)
    let p2 = Point3d(p1.X, p1.Y + t - (b - d) / 20. - tan (ang / 2.) * r1, 0.)
    let p3 = Point3d(p2.X - r1 + cos (ang) * r1, p2.Y + sin (ang) * r1, 0.)

    let p5 = Point3d(p0.X + d, p0.Y + t + (b - d) / 20. + tan (ang / 2.) * r, 0.)
    let p4 = Point3d(p5.X + r - cos (ang) * r, p5.Y - sin (ang) * r, 0.)

    let mat1 =
        Matrix3d.Mirroring(new Line3d(Point3d(p0.X, p0.Y + h / 2., 0.), Point3d(p0.X + 1., p0.Y + h / 2., 0.)))

    let p6 = p5.TransformBy(mat1)
    let p7 = p4.TransformBy(mat1)
    let p8 = p3.TransformBy(mat1)
    let p9 = p2.TransformBy(mat1)
    let p10 = p1.TransformBy(mat1)
    let p11 = Point3d(p0.X, p0.Y + h, 0.)

    seq {
        (p0, 0., 0., 0.)
        (p1, 0., 0., 0.)
        (p2, 0.385314, 0, 0)
        (p3, 0, 0, 0)
        (p4, -0.385314, 0, 0)
        (p5, 0, 0, 0)
        (p6, -0.385314, 0, 0)
        (p7, 0, 0, 0)
        (p8, 0.385314, 0, 0)
        (p9, 0, 0, 0)
        (p10, 0, 0, 0)
        (p11, 0, 0, 0)

    }

/// <summary>工字钢</summary>
/// <param name="p0">基点</param>
/// <param name="h">高度</param>
/// <param name="b">腿宽</param>
/// <param name="d">腰厚</param>
/// <param name="t">平均腰厚</param>
/// <param name="r">内圆弧半径</param>
/// <param name="r1">腿端圆弧半径</param>
/// <returns></returns>
let gongzigang (p0: Point3d) h b d t r r1 =

    let ang = atan 6.0

    let p1 = Point3d(p0.X - b / 2., p0.Y, 0)
    let p2 = Point3d(p1.X, p1.Y + t - (b - d) / 24. - tan (ang / 2.) * r1, 0.)
    let p3 = Point3d(p2.X + r1 - cos (ang) * r1, p2.Y + sin (ang) * r1, 0.)

    let p5 = Point3d(p0.X - d / 2., p0.Y + t + (b - d) / 24. + tan (ang / 2.) * r, 0.)

    let p4 = Point3d(p5.X - r + cos (ang) * r, p5.Y - sin (ang) * r, 0.)

    let mat1 =
        Matrix3d.Mirroring(new Line3d(Point3d(p0.X, p0.Y + h / 2., 0.), Point3d(p0.X + 1., p0.Y + h / 2., 0.)))

    let p6 = p5.TransformBy(mat1)
    let p7 = p4.TransformBy(mat1)
    let p8 = p3.TransformBy(mat1)
    let p9 = p2.TransformBy(mat1)
    let p10 = p1.TransformBy(mat1)

    let mat2 = Matrix3d.Mirroring(new Line3d(p0, Point3d(p0.X, p0.Y + 1., 0.)))

    let p16 = p5.TransformBy(mat2)
    let p17 = p4.TransformBy(mat2)
    let p18 = p3.TransformBy(mat2)
    let p19 = p2.TransformBy(mat2)
    let p20 = p1.TransformBy(mat2)


    let p11 = p10.TransformBy(mat2)
    let p12 = p9.TransformBy(mat2)
    let p13 = p8.TransformBy(mat2)
    let p14 = p7.TransformBy(mat2)
    let p15 = p6.TransformBy(mat2)

    seq {
        (p1, 0., 0., 0.)
        (p2, -0.366629, 0, 0)
        (p3, 0, 0, 0)
        (p4, 0.366629, 0, 0)
        (p5, 0, 0, 0)
        (p6, 0.366629, 0, 0)
        (p7, 0, 0, 0)
        (p8, -0.366629, 0, 0)
        (p9, 0, 0, 0)
        (p10, 0, 0, 0)
        (p11, 0, 0, 0)
        (p12, -0.366629, 0, 0)
        (p13, 0, 0, 0)
        (p14, 0.366629, 0, 0)
        (p15, 0, 0, 0)
        (p16, 0.366629, 0, 0)
        (p17, 0, 0, 0)
        (p18, -0.366629, 0, 0)
        (p19, 0, 0, 0)
        (p20, 0, 0, 0)
    }
