# FCad

## 介绍
用f#来写cad的插件

本项目感谢Gile大神的初始代码，[项目地址](https://github.com/gileCAD/AutoCAD-Fsharp-Project-Template)
## 软件架构
参考 [IFoxCAD 项目](https://gitee.com/inspirefunction/ifoxcad) 的代码结构进行

## 计算表达式
[计算表达式](ce.md)