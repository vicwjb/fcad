本文引自[gaoshoufenmu的博客](https://www.cnblogs.com/sjjsxl/)
# [Computation expressions: Introduction](https://www.cnblogs.com/sjjsxl/p/4978263.html)

本文仅为对原文的翻译，主要是记录以方便以后随时查看。原文地址为[http://fsharpforfunandprofit.com/posts/computation-expressions-intro](http://fsharpforfunandprofit.com/posts/computation-expressions-intro)

## 背景

是时候揭开计算表达式（ **Computation expression** ）的神秘面纱了。现有的解释说明都令人难以理解。比如查阅[MSDN官方说明](http://msdn.microsoft.com/en-us/library/dd233182.aspx)，则对初学者来说虽然简单明确，却对理解没有什么太大帮助。例如当你看到如下代码

`{| let! pattern = expr in cexpr |}`

它只是如下方法调用的一个简单的语法糖：

`builder.Bind(expr, (fun pattern -> {| cexpr |}))`

但，这是个什么鬼？

## 实战

首先看一段简单的代码，然后再用 **Computation expression** 实现同样的功能。



``` f#
let log p = printfn "expression is %A" p

let loggedWorkflow = 
    let x = 42
    log x
    let y = 43
    log y
    let z = x + y
    log z
    //return
    z
```

运行结果为

``` f#
expression is 42
expression is 43
expression is 85
```

看起来很简单，但是每次都显式的调用 **log** 语句还是挺烦的，考虑隐藏这些 **log** 语句的方法，这时候， **Computation expression** 派上用场。

首先定义一个类型


```f#
type LoggingBuilder() =
    let log p = printfn "expression is %A" p

    member this.Bind(x, f) = 
        log x
        f x

    member this.Return(x) = 
        x
```

先不用管这段代码中的 **Bind ** 和 ** Return** 成员方法，后面会给出解释。接着看如下代码，实现刚才的 **log** 功能


```f#
let logger = new LoggingBuilder()

let loggedWorkflow = 
    logger
        {
        let! x = 42
        let! y = 43
        let! z = x + y
        return z
        }
```

运行这段代码可以获得跟刚才同样的输出结果，但是很明显，刚才代码中重复的 **log** 语句已经被隐藏了。

## 安全除法

现在让我们看一个经典的例子。

假如要除以一系列的数，即一个接一个将这些数作为除数，但是这些数其中可能有  **0**  。如何处理？抛出一个异常会使代码丑陋，使用 **option** 类型好像是一个不错的方法。

先定义一个帮助函数，实现除法功能并返回一个 **int option** ，正常情况下则为 **Some** ，否则为 **None** 。然后将这些除法过程链接起来，并且在每个除法过程后判定除法是否成功（返回 **Some** ），只有在成功的时候才会继续下一个除法过程。帮助函数如下

```f#
let divideBy bottom top =
    if bottom = 0
    then None
    else Some(top/bottom)
```

注意第一个参数为除数，故我们可以将除法表达式写成  **12 |> divideBy 3**  （表示 **12/3** ）的形式，这样更容易将整个除法过程串联起来。

看一个具体的实例，用三个数依次去除一个初始数


```f#
let divideByWorkflow init x y z = 
    let a = init |> divideBy x
    match a with
    | None -> None  // give up
    | Some a' ->    // keep going
        let b = a' |> divideBy y
        match b with
        | None -> None  // give up
        | Some b' ->    // keep going
            let c = b' |> divideBy z
            match c with
            | None -> None  // give up
            | Some c' ->    // keep going
                //return 
                Some c'
```

函数调用

```f#
let good = divideByWorkflow 12 3 2 1
let bad = divideByWorkflow 12 3 0 1
```
 **bad** 变量为 **None** ，因为有一个除数为  **0**  

注意到以上例子中，返回结果必须为 **int option** ，不能返回 **int** 。然后，这个例子中的连续测试除法是否失败的代码依然丑陋，考虑使用 **computation expression** 。

我们定义一个新的类型如下，并实例化



```f#
type MaybeBuilder() =

    member this.Bind(x, f) = 
        match x with
        | None -> None
        | Some a -> f a

    member this.Return(x) = 
        Some x
   
let maybe = new MaybeBuilder()
```

重写除以一系列数的工作流的函数，隐藏了之前的判断分支逻辑


```f#
let divideByWorkflow init x y z = 
    maybe 
        {
        let! a = init |> divideBy x
        let! b = a |> divideBy y
        let! c = b |> divideBy z
        return c
        } 
```

测试以上函数，可得到同样的结果

```f
let good = divideByWorkflow 12 3 2 1   
  
let bad = divideByWorkflow 12 3 0 1
```

## 链接“or else” 测试

上一个例子中，我们在某一步除法执行成功后才继续执行下一个除法，但是，有时候的控制流程并非如此，而是“or else”，即，第一个事情没有成功，则尝试第二个事情，第二个事情如果也失败，则尝试第三个事情，依次类推。

看一个简单例子。假设我们有三个字典，并且我们想查找某一键对应的值，查询每一个字典的结果可能是成功或者失败。



```f#
let map1 = [ ("1","One"); ("2","Two") ] |> Map.ofList
let map2 = [ ("A","Alice"); ("B","Bob") ] |> Map.ofList
let map3 = [ ("CA","California"); ("NY","New York") ] |> Map.ofList

let multiLookup key =
    match map1.TryFind key with
    | Some result1 -> Some result1   // success
    | None ->   // failure
        match map2.TryFind key with
        | Some result2 -> Some result2 // success
        | None ->   // failure
            match map3.TryFind key with
            | Some result3 -> Some result3  // success
            | None -> None // failure
```



这个查询函数的使用如下

```f#
multiLookup "A" |> printfn "Result for A is %A" 
multiLookup "CA" |> printfn "Result for CA is %A" 
multiLookup "X" |> printfn "Result for X is %A"
```

代码运行良好，但同样查询函数multiLookup的定义代码太烦，简化一下，首先定义一个bulider类如下



```f#
type OrElseBuilder() =
    member this.ReturnFrom(x) = x
    member this.Combine (a,b) = 
        match a with
        | Some _ -> a  // a succeeds -- use it
        | None -> b    // a fails -- use b instead
    member this.Delay(f) = f()

let orElse = new OrElseBuilder()
```



重写查询函数



```f#
let map1 = [ ("1","One"); ("2","Two") ] |> Map.ofList
let map2 = [ ("A","Alice"); ("B","Bob") ] |> Map.ofList
let map3 = [ ("CA","California"); ("NY","New York") ] |> Map.ofList

let multiLookup key = orElse {
    return! map1.TryFind key
    return! map2.TryFind key
    return! map3.TryFind key
    }
```



使用示例代码如下，运行后发现结果同期望一样

```f#
multiLookup "A" |> printfn "Result for A is %A" 
multiLookup "CA" |> printfn "Result for CA is %A" 
multiLookup "X" |> printfn "Result for X is %A"
```

## 带回调的异步调用

.net中异步操作的标准方法是使用[AsyncCallback delegate](http://msdn.microsoft.com/en-us/library/ms228972.aspx)，这在异步操作完成时被调用。

举个例子，网页下载  



```f#
open System.Net
let req1 = HttpWebRequest.Create("http://tryfsharp.org")
let req2 = HttpWebRequest.Create("http://google.com")
let req3 = HttpWebRequest.Create("http://bing.com")

req1.BeginGetResponse((fun r1 ->    // 请求1异步获取响应，完成后，请求2异步获取响应，完成后，请求3异步获取响应
    use resp1 = req1.EndGetResponse(r1)
    printfn "Downloaded %O" resp1.ResponseUri

    req2.BeginGetResponse((fun r2 -> 
        use resp2 = req2.EndGetResponse(r2)
        printfn "Downloaded %O" resp2.ResponseUri

        req3.BeginGetResponse((fun r3 -> 
            use resp3 = req3.EndGetResponse(r3)
            printfn "Downloaded %O" resp3.ResponseUri

            ),null) |> ignore
        ),null) |> ignore
    ),null) |> ignore
```



以上代码使用太多的BeginGetResponse和EndGetResponse，以及嵌套的lambda，使得代码阅读费力。

事实上，在需要连接回调函数的代码中，管理这种级联方法总是显得困难，这甚至被称为 ["Pyramid of Doom"]  **原文链接失效** 

当然，在F#中我们将不再写类似的代码，因为F#有内建的 **async computation expression**  ，这简化了代码。



```f#
open System.Net
let req1 = HttpWebRequest.Create("http://tryfsharp.org")
let req2 = HttpWebRequest.Create("http://google.com")
let req3 = HttpWebRequest.Create("http://bing.com")

async {
    use! resp1 = req1.AsyncGetResponse()  
    printfn "Downloaded %O" resp1.ResponseUri

    use! resp2 = req2.AsyncGetResponse()  
    printfn "Downloaded %O" resp2.ResponseUri

    use! resp3 = req3.AsyncGetResponse()  
    printfn "Downloaded %O" resp3.ResponseUri

    } |> Async.RunSynchronously
```



在这个系列的后面部分会看到  **async**  工作流是如何实现的。

## 总结

至此我们见到一些简单的 **computation expression** 的例子。

*   logging例子中，我们想在每一步中添加一些自定义的逻辑，如打印log信息。
*   安全除法例子中，我们想更为优雅的处理除法出错的情况，以便我们更加专注其他一些事情。
*   在多字典查询例子中，我们想在第一次查询字典成功后就结束并返回。
*   最后在异步操作例子中，我们想隐藏大段的回调函数的代码。

这些例子的一个共同点就是在每个表达式中， **computation expression** 做了一些后台的事情。

打一个不是很好的比方，可以把 **computation expression** 想象成 **SVN** 或者 **Git** 的一个提交后钩子，或者数据库的每次更新后被调用的触发器。这就是 **computation expression** ，它可以隐藏一些代码，从而让我们更专注于业务逻辑。

至于 **computation expression ** 和 ** workflow** （工作流）之间的区别，我使用 **computation expression** 表示  **{...}**  和  **let** ！语法，而 **workflow** （工作流）表示具体实现。当然，不是所有 **computation expression** 的实现都是工作流，例如，说“ **async** ”工作流或者“ **maybe** ”工作流是合适的，但是说“seq”工作流就显得不太合适。

也就是说，如下面的代码



```f#
maybe 
    {
    let! a = x |> divideBy y 
    let! b = a |> divideBy w
    let! c = b |> divideBy z
    return c
    } 
```



可以说 **maybe** 是我们使用的工作流，而  **{ let! a = .... return c } ** 是 ** computation expression** 

 **附：** 

讲[state type](http://fsharpforfunandprofit.com/posts/monadster/)的文章。


# [Understanding continuations](https://www.cnblogs.com/sjjsxl/p/4980429.html)

原文地址[http://fsharpforfunandprofit.com/posts/computation-expressions-continuations/](http://fsharpforfunandprofit.com/posts/computation-expressions-continuations/)

上一篇中我们看到复杂代码是如何通过使用 **computation expressions** 得到简化。

使用 **computation expression** 前的代码



```f#
let log p = printfn "expression is %A" p

let loggedWorkflow = 
    let x = 42
    log x
    let y = 43
    log y
    let z = x + y
    log z
    //return
    z
```



使用 **computation expression** 后的代码



```f#
let loggedWorkflow = 
    logger
        {
        let! x = 42
        let! y = 43
        let! z = x + y
        return z
        }
```



这里使用的是 **let!（let bang）** 而非普通的 **let** 。我们是否可以自己来模拟 **let!** 从而深入理解这里面到底发生了什么？当然可以，不过我们首先需要搞懂什么是 **continuations** 

## Continuations

命令式编程中，一个函数中有“ **returning** ”的概念。当调用一个函数时，先进入这个函数，然后退出，类似于入栈和出栈。

典型的C#代码



```c#
public int Divide(int top, int bottom)
{
    if (bottom==0)
    {
        throw new InvalidOperationException("div by 0");
    }
    else
    {
        return top/bottom;
    }
}

public bool IsEven(int aNumber)
{
    var isEven = (aNumber % 2 == 0);
    return isEven;
}
```



这样的函数我们已经很熟悉了，但是也许我们未曾考虑过这样一个问题：被调用的函数本身就决定了要做什么事情。

例如，函数 **Divide** 的实现就决定了可能会抛出一个异常，但如果我们不想得到一个异常呢？也许是想得到一个 **nullable\<int>** ，又或者是将有关信息打印到屏幕如" **#DIV/0** "。简单来说，为什么不能由函数调用者而非函数自身来决定应该怎么处理。

 **IsEven** 函数也是如此，对于其返回的 **boolean** 类型的值我们将如何处理？对它进行分支讨论，还是打印到报告中？比起返回一个 **boolean** 类型的值让调用者来处理，为什么不考虑让调用者告诉被调用方下一步如何处理。

这就是 **continuation** ，它是一个简单的函数，你可以向其中传入另一个函数来指示下一步该做什么。

重写刚才的C#代码，由调用方传入一个函数，被调用方用这个函数来处理一些事情。



```c#
public T Divide<T>(int top, int bottom, Func<T> ifZero, Func<int,T> ifSuccess)
{
    if (bottom==0)
    {
        return ifZero();
    }
    else
    {
        return ifSuccess( top/bottom );
    }
}

public T IsEven<T>(int aNumber, Func<int,T> ifOdd, Func<int,T> ifEven)
{
    if (aNumber % 2 == 0)
    {
        return ifEven(aNumber);
    }
    else
    {   return ifOdd(aNumber);
    }
}
```



嗯，在C#中，传入太多的 **Func** 类型的参数会使代码丑陋，所以C#中这不常见，但是在F#中使用则非常简单。

使用 **continuations** 前的代码



```f#
let divide top bottom = 
    if (bottom=0) 
    then invalidOp "div by 0"
    else (top/bottom)
    
let isEven aNumber = 
    aNumber % 2 = 0
```



使用 **continuations** 后的代码



```f#
let divide ifZero ifSuccess top bottom = 
    if (bottom=0) 
    then ifZero()
    else ifSuccess (top/bottom)
    
let isEven ifOdd ifEven aNumber = 
    if (aNumber % 2 = 0)
    then aNumber |> ifEven 
    else aNumber |> ifOdd
```



有几点说明。首先，函数参数靠前，如 **isZero** 作为第一个参数， **isSuccess** 作为第二个参数。这会方便我们使用函数的部分应用 [partial application](http://fsharpforfunandprofit.com/posts/partial-application/)。

其次，在 **isEven** 例子中， **aNumber |> ifEven ** 和 ** aNumber |> ifOdd** 的写法表明我们将当前值加入到 **continuation** 的管道中，这篇文章的后面部分也会使用相同的模式。

## Continuation的例子

采用推荐的 **Continuation** 方式，我们可以用三种不同的方式调用 **divide** 函数，这取决于调用者的意图。

以下是调用 **divide** 函数的三个应用场景：

1、将结果加入到消息管道并且打印

2、将结果转换成 **option** ，除法失败时返回 **None** ，除法成功时返回 **Some** 

3、除法失败时抛出异常，除法成功时返回结果值



```f#
// Scenario 1: pipe the result into a message
// ----------------------------------------
// setup the functions to print a message
let ifZero1 () = printfn "bad"
let ifSuccess1 x = printfn "good %i" x

// use partial application
let divide1  = divide ifZero1 ifSuccess1

//test
let good1 = divide1 6 3
let bad1 = divide1 6 0

// Scenario 2: convert the result to an option
// ----------------------------------------
// setup the functions to return an Option
let ifZero2() = None
let ifSuccess2 x = Some x
let divide2  = divide ifZero2 ifSuccess2

//test
let good2 = divide2 6 3
let bad2 = divide2 6 0

// Scenario 3: throw an exception in the bad case
// ----------------------------------------
// setup the functions to throw exception
let ifZero3() = failwith "div by 0"
let ifSuccess3 x = x
let divide3  = divide ifZero3 ifSuccess3

//test
let good3 = divide3 6 3
let bad3 = divide3 6 0

```


现在，调用者不需要到处对 **divide** 抓异常了。调用者决定一个异常是否被抛出，而不是被调用的函数决定是否抛出一个异常。即，如果调用者想对除法失败时抛出异常处理，则将 **isZero3** 函数传入到 **divide** 函数中。

同样的三个调用 **isEven** 的场景类似处理



```f#
// Scenario 1: pipe the result into a message
// ----------------------------------------
// setup the functions to print a message
let ifOdd1 x = printfn "isOdd %i" x
let ifEven1 x = printfn "isEven %i" x

// use partial application
let isEven1  = isEven ifOdd1 ifEven1

//test
let good1 = isEven1 6 
let bad1 = isEven1 5

// Scenario 2: convert the result to an option
// ----------------------------------------
// setup the functions to return an Option
let ifOdd2 _ = None
let ifEven2 x = Some x
let isEven2  = isEven ifOdd2 ifEven2

//test
let good2 = isEven2 6 
let bad2 = isEven2 5

// Scenario 3: throw an exception in the bad case
// ----------------------------------------
// setup the functions to throw exception
let ifOdd3 _ = failwith "assert failed"
let ifEven3 x = x
let isEven3  = isEven ifOdd3 ifEven3

//test
let good3 = isEven3 6 
let bad3 = isEven3 5
```



此时，调用者不用到处对返回的 **boolean** 类型值使用 **if/then/else** 结构处理了，调用者想怎么处理，就将对应的处理函数作为参数传入 **divide** 函数中即可。

在 [designing with types](http://fsharpforfunandprofit.com/posts/designing-with-types-single-case-dus/)中，我们已经见识过 **continuations** 了。



```f#
type EmailAddress = EmailAddress of string

let CreateEmailAddressWithContinuations success failure (s:string) = 
    if System.Text.RegularExpressions.Regex.IsMatch(s,@"^\S+@\S+\.\S+$") 
        then success (EmailAddress s)
        else failure "Email address must contain an @ sign"
```



测试代码如下



```f#
// setup the functions 
let success (EmailAddress s) = printfn "success creating email %s" s        
let failure  msg = printfn "error creating email: %s" msg
let createEmail = CreateEmailAddressWithContinuations success failure

// test
let goodEmail = createEmail "x@example.com"
let badEmail = createEmail "example.com"
```



## Continuation passing style

使用 **continuation** 将产生一种编程风格"[continuation passing style](http://en.wikipedia.org/wiki/Continuation-passing_style)" (CPS)，其中，调用每个函数时，都有一个函数参数指示下一步做什么。

为了看到区别，我们首先看一个标准的直接风格的编程，进入和退出一个函数，如



```f#
call a function ->
   <- return from the function
call another function ->
   <- return from the function
call yet another function ->
   <- return from the function
```



在CPS中，则是用一个函数链



```f#
evaluate something and pass it into ->
   a function that evaluates something and passes it into ->
      another function that evaluates something and passes it into ->
         yet another function that evaluates something and passes it into ->
            ...etc...
```



直接风格的编程中，存在一个函数之间的层级关系。最顶层的函数类似于“主控制器”，调用一个分支，然后另一个，决定何时进行分支，何时进行循环等。

在CPS中，则不存在这样一个“主控制器”，而是有一个“管道”的控制流，负责单个任务的函数在其中按顺序依次执行。

如果你曾在GUI中附加一个事件句柄（event handler）到一个按钮单击，或者通过[BeginInvoke](http://msdn.microsoft.com/en-us/library/2e08f6yc.aspx)使用回调，那你已经在无意中使用了CPS。事实上，这种风格是理解 **async** 工作流的关键，这一点会在以后的文章中讨论。

## Continuations and 'let'

以上讨论的CPS等在 **let** 内部是如何组合的？

首先回顾（[revisit](http://fsharpforfunandprofit.com/posts/let-use-do/)）一下  **let**  的本质。

记住非顶级的let不能被外界访问，它总是作为其外层代码块的一部分存在。

`let x = someExpression`

真正含义是

`let x = someExpression in [an expression involving x]`

然后每次在第二个表达式（主体表达式）见到  **x**  ，用第一个表达式（ **someExpression** ）替换。例如

```f#
let x = 42
let y = 43
let z = x + y
```

其实是指（使用in关键字）

```f#
let x = 42 in   
  let y = 43 in 
    let z = x + y in
       z    // the result
```

有趣的是，有一个类似 **let ** 的 ** lambda** 

`fun x -> [an expression involving x]`

如果我们把x加入管道，则

`someExpression |> (fun x -> [an expression involving x] )`

是不是非常像 **let** ？下面是一个 **let** 和一个 **lambda** 



```f#
// let
let x = someExpression in [an expression involving x]

// pipe a value into a lambda
someExpression |> (fun x -> [an expression involving x] )
```



两者都有  **x**  和 **someExpression** ，在 **lambda** 的主体的任何地方只有见到  **x**  就将它替换成 **someExpression** 。嗯，在 **lambda** 中，  **x**  和 **someExpression** 仅仅是位置反过来了，否则基本跟 **let** 基本一样了。

所以，我们也可以写成如下形式

```f#
42 |> (fun x ->
  43 |> (fun y -> 
     x + y |> (fun z -> 
       z)))
```

当写成这种形式时，我们已经将 **let** 风格转变成CPS了。

代码说明：

*   第一行我们获取值42——如何处理？将它传入一个 **continuation** ，正如前面我们对isEven函数所做的一样。在此处的 **continuation** 上下文中，我们将42重写标为  **x**  
*   第二行我们有值43——如何处理？将它传入一个 **continuation** ，在这个上下文中，将它重新标为  **y**  
*   第三行我们把  **x**  和  **y**  加在一起创建一个新值——如何处理这个新值？再来一个 **continuation** ，并且再来一个标签  **z**  指示这个新值
*   最后完成并且整个表达式计算结果为  **z**  

包装 **continuation** 到一个函数中

我们想避开显式的管道操作(|>)，而是用一个函数来包装这个逻辑。无法称这个函数为 **let** 因为 **let** 是一个保留字，更重要的是， **let** 的参数位置是反过来的。注意，  **x**  在右边而 **someExpression** 在左边，所以现在称此函数为“ **pipeInto** ”，定义如下

```f#
let pipeInto (someExpression,lambda) =
    someExpression |> lambda
```

注意这里参数是一个元组而非两个独立的参数。使用pipeInto函数，我们可以重写上面的代码为

```f#
pipeInto (42, fun x ->
  pipeInto (43, fun y -> 
    pipeInto (x + y, fun z -> 
       z)))
```

去掉行首缩进则为

```f#
pipeInto (42, fun x ->
pipeInto (43, fun y -> 
pipeInto (x + y, fun z -> 
z)))
```

也许你会认为：几个意思？为啥要包装管道符为一个函数咧？

答案是这样，我们可以添加额外的代码到 **pipeInto** 函数中来处理一些幕后事情，正如 **computation expression** 那样。

回顾“logging”例子

重新定义 **pipeInto** ，增加一个 **logging** 功能，如下

```f#
let pipeInto (someExpression,lambda) =
   printfn "expression is %A" someExpression 
   someExpression |> lambda
```

如此，本篇一开始的代码则可重写为



```f#
pipeInto (42, fun x ->
pipeInto (43, fun y -> 
pipeInto (x + y, fun z -> 
z
)))
```



这段代码的输出

```f#
expression is 42
expression is 43
expression is 85
```

这跟早期的实现，输出结果相同。至此，我们已经实现了自己的小小的 **computation expression**  工作流。

如果我们将这个 **pipeInto** 实现与 **computation expression** 版本比较，我们可以发现我们自己写的版本是非常接近 **let** ！的，除了将参数位置反过来了，还有就是有为了 **continuation** 而显式写的  **->**  符号。

![](http://fsharpforfunandprofit.com/assets/img/compexpr_logging.png)

## 回顾“安全除法”例子

先给出原先的代码



```f#
let divideBy bottom top =
    if bottom = 0
    then None
    else Some(top/bottom)

let divideByWorkflow x y w z = 
    let a = x |> divideBy y 
    match a with
    | None -> None  // give up
    | Some a' ->    // keep going
        let b = a' |> divideBy w
        match b with
        | None -> None  // give up
        | Some b' ->    // keep going
            let c = b' |> divideBy z
            match c with
            | None -> None  // give up
            | Some c' ->    // keep going
                //return 
                Some c'
```



看看是否可以将额外的代码加入 **pipeInto** 函数。我们想要的逻辑是

如果 **someExpression** 参数为 **None** ，则不调用 **continuation lambda** 

如果 **someExpression** 参数是 **Some** ，则调用 **continuation lambda** ，传入 **Some** 的内容

逻辑实现如下



```f#
let pipeInto (someExpression,lambda) =
   match someExpression with
   | None -> 
       None
   | Some x -> 
       x |> lambda
```



使用这个版本的 **pipeInto** 函数，可以重写刚才的原始代码



```f#
let divideByWorkflow x y w z = 
    let a = x |> divideBy y 
    pipeInto (a, fun a' ->
        let b = a' |> divideBy w
        pipeInto (b, fun b' ->
            let c = b' |> divideBy z
            pipeInto (c, fun c' ->
                Some c' //return 
                )))
```



可以将这段代码再简化一下。首先去除 **a，b，c** ，用 **divideBy** 表达式代替，即

```f#
let a = x |> divideBy y 
pipeInto (a, fun a' ->
```

变成

```f#
pipeInto (x |> divideBy y, fun a' ->
```

将a'重新标记为a，b和c类似处理，去除行首缩进，则代码变成



```f#
let divideByResult x y w z = 
    pipeInto (x |> divideBy y, fun a ->
    pipeInto (a |> divideBy w, fun b ->
    pipeInto (b |> divideBy z, fun c ->
    Some c //return 
    )))

```


最后，我们定义一个帮助函数 **return** 用以包装一个值为一个 **option** ，全部代码如下



```f#
let divideBy bottom top =
    if bottom = 0
    then None
    else Some(top/bottom)

let pipeInto (someExpression,lambda) =
   match someExpression with
   | None -> 
       None
   | Some x -> 
       x |> lambda 

let return' c = Some c

let divideByWorkflow x y w z = 
    pipeInto (x |> divideBy y, fun a ->
    pipeInto (a |> divideBy w, fun b ->
    pipeInto (b |> divideBy z, fun c ->
    return' c 
    )))

let good = divideByWorkflow 12 3 2 1
let bad = divideByWorkflow 12 3 0 1
```



比较我们自己实现的版本与 **computation expression** 版本，发现仅仅语法不同

![查看图片](http://fsharpforfunandprofit.com/assets/img/compexpr_safedivide.png)

## 总结

这篇文章中，我们讨论了 **continuation ** 和 ** continuation passing style** （CPS），以及为什么认为 **let** 是一个优秀的语法，因为 **let** 在后台进行了 **continuation** 处理。

现在我们可以定义自己的 **let** 版本，下一篇我们将把这些付诸实际。



# [Introducing 'bind'](https://www.cnblogs.com/sjjsxl/p/4983076.html)

原文地址：[http://fsharpforfunandprofit.com/posts/computation-expressions-bind/](http://fsharpforfunandprofit.com/posts/computation-expressions-bind/)

上一篇讨论了如何理解 **let** 作为一个能实现 **continuations** 功能的语法，并介绍了 **pipeInto** 函数能让我们增加钩子（处理逻辑）到 **continuation** 管道。

现在可以来一探究竟第一个builder方法—— **Bind** ，它是 **computation expression** 的核心。

## 介绍“Bind

[MSDN的computation expression](https://msdn.microsoft.com/en-us/library/dd233182.aspx)说明了  **let!**  表达式是  **Bind**  方法的语法糖。请看  **let!**  文档说明以及一个示例



```f#
// documentation
{| let! pattern = expr in cexpr |}

// real example
let! x = 43 in some expression
```



 **Bind** 方法文档说明及示例



```f#
// documentation
builder.Bind(expr, (fun pattern -> {| cexpr |}))

// real example
builder.Bind(43, (fun x -> some expression))
```



 **Bind** 有两个参数，一个表达式（ **43** ）和一个 **lambda** 

 **lambda** 的参数  **x**  绑定到 **Bind** 的第一个参数

 **Bind** 的参数顺序与 **let** ！的参数顺序相反

将 **let** ！表达式链接起来

```f#
let! x = 1
let! y = 2
let! z = x + y
```

编译器会转换成调用 **Bind** ，就像这样

```f#
Bind(1, fun x ->
Bind(2, fun y ->
Bind(x + y, fun z ->
etc
```

我想你会发现有似曾相识的感觉。没错， **pipeInto** 函数跟 **Bind** 函数完全相同。

实际上， **computation expressions**  仅是一种创建语法糖的方式，以让我们自己来实现想做的事情。

## "bind" 函数

“ **bind** ”是一个标准的函数模式，不依赖于 **computation expression** 。

首先，为何称之为“ **bind** ”？嗯，正如我们所见，“ **bind** ”函数可被想象成将一个输入值传入到一个函数，即绑定一个值到一个函数的参数上。可见“ **bind** ”类似于管道或组合。

事实上，可以将它转成一个中缀操作

`let (>>=) m f = pipeInto(m,f)`

顺便一提，“ **>>=** ”是将 **bind** 写成中缀操作符的标准方式。如果你曾在F#代码中见过这个符号，这个符号的意思就是这里绑定的含义。

再看之前“安全除法”的例子，现在可以将代码写成如下

```f#
let divideByWorkflow x y w z = 
    x |> divideBy y >>= divideBy w >>= divideBy z
```

这种方式与普通的管道或者组合的区别不是很明显，但是有两点值得一提

*   首先， **bind** 函数在每个场景下有额外的自定义行为（如前面文章中logging和安全除法的例子，分别有额外的打印log和对除数是否为0的判断），它不像管道或者组合，它不是一个泛型函数
*   其次，输入参数类型（如上面的  **m**  ）不一定与函数参数（如上面的  **f**  ）的输出类型相同，因此这个 **bind** 所做的事情就是优雅的处理这个不匹配以让函数可以被链接起来

我们可以在下一篇看到， **bind** 是与某种“包装（ **wrapper** ）类型”配合使用，它的值参数可能是 **WrapperType&lt;TypeA> ** ， ** bind** 函数的函数参数的签名总是 **TypeA**  ->  **WrapperType&lt;TypeB>** 。

在“安全除法”的例子中，包装类型是 **Option** 。值参数（对应上面的  **m**  ）的类型是 **Option&lt;int>** ，函数参数（对应上面的f）的签名是 **int -> Option&lt;int>** 。

再看一个例子，其中使用了中缀绑定函数


```f#

let (>>=) m f = 
    printfn "expression is %A" m
    f m

let loggingWorkflow = 
    1 >>= (+) 2 >>= (*) 42 >>= id
```



这个例子中，没有包装类型。所有的都是 **int** 类型。但即使如此， **bind** 也有幕后打印 **logging** 的这种行为。

## Option.bind 和回顾"maybe" 工作流


F#库中，你可以在很多地方看到 **Bind** 函数。现在你知道它们是什么。

一个特别有用的例子是 **Option.bind** ，这跟我们上面写的代码的功能相同，即

如果输入参数为 **None** ，那不再调用 **continuation** 函数。

如果输入参数为 **Some** ，那调用 **continuation** 函数，并将 **Some** 的内容作为参数传入到函数中。

下面是我们自己写的函数



```f#
let pipeInto (m,f) =
   match m with
   | None -> 
       None
   | Some x -> 
       x |> f
```



然后是Option.bind的实现


```f#

module Option = 
    let bind f m =
       match m with
       | None -> 
           None
       | Some x -> 
           x |> f 
```



用 **Option.bind** 重写“ **maybe** ”工作流

```f#
type MaybeBuilder() =
    member this.Bind(m, f) = Option.bind f m
    member this.Return(x) = Some x
```

## 复习不同的实现方法

迄今已经用四种不同的方法实现“安全除法”。将它们放在一起，并作比较

首先是最原始的版本，它使用了一个显式的工作流



```f#
module DivideByExplicit = 

    let divideBy bottom top =
        if bottom = 0
        then None
        else Some(top/bottom)

    let divideByWorkflow x y w z = 
        let a = x |> divideBy y 
        match a with
        | None -> None  // give up
        | Some a' ->    // keep going
            let b = a' |> divideBy w
            match b with
            | None -> None  // give up
            | Some b' ->    // keep going
                let c = b' |> divideBy z
                match c with
                | None -> None  // give up
                | Some c' ->    // keep going
                    //return 
                    Some c'
    // test
    let good = divideByWorkflow 12 3 2 1
    let bad = divideByWorkflow 12 3 0 1
```

其次，使用我们自己定义的函数的版本（也就是“ **pipeInto** ”）

```f#
module DivideByWithBindFunction = 

    let divideBy bottom top =
        if bottom = 0
        then None
        else Some(top/bottom)

    let bind (m,f) =
        Option.bind f m

    let return' x = Some x
       
    let divideByWorkflow x y w z = 
        bind (x |> divideBy y, fun a ->
        bind (a |> divideBy w, fun b ->
        bind (b |> divideBy z, fun c ->
        return' c 
        )))

    // test
    let good = divideByWorkflow 12 3 2 1
    let bad = divideByWorkflow 12 3 0 1
```

然后，使用 **computation expression** 的版本

```f#
module DivideByWithCompExpr = 

    let divideBy bottom top =
        if bottom = 0
        then None
        else Some(top/bottom)

    type MaybeBuilder() =
        member this.Bind(m, f) = Option.bind f m
        member this.Return(x) = Some x

    let maybe = new MaybeBuilder()

    let divideByWorkflow x y w z = 
        maybe 
            {
            let! a = x |> divideBy y 
            let! b = a |> divideBy w
            let! c = b |> divideBy z
            return c
            }    

    // test
    let good = divideByWorkflow 12 3 2 1
    let bad = divideByWorkflow 12 3 0 1
```

最后，用中缀操作来实现绑定

```f#
module DivideByWithBindOperator = 

    let divideBy bottom top =
        if bottom = 0
        then None
        else Some(top/bottom)

    let (>>=) m f = Option.bind f m

    let divideByWorkflow x y w z = 
        x |> divideBy y 
        >>= divideBy w 
        >>= divideBy z 

    // test
    let good = divideByWorkflow 12 3 2 1
    let bad = divideByWorkflow 12 3 0 1
```

 **bind** 函数是非常强大的。下一篇我们将会看到结合 **bind** 和包装类型来创造一种优雅的方式，并用这种方式传递额外的信息。

## 练习：你能理解多少？

在进入下一篇之前，何不来测试下自己是否已经理解之前的内容？

### 第一部分——创建一个工作流

首先，创建一个函数，将字符串解析成 **int** 型：

`let strToInt str = ???`

然后，创建一个 **computation expression**  builder类，用在工作流，如下所示

```f#
let stringAddWorkflow x y z = 
    yourWorkflow 
        {
        let! a = strToInt x
        let! b = strToInt y
        let! c = strToInt z
        return a + b + c
        }    

// test code
let good = stringAddWorkflow "12" "3" "2"
let bad = stringAddWorkflow "12" "xyz" "2"
```



解析：

 **strToInt** 函数类似上面的 **divide** 函数，故可以写出函数定义如下



```f#
open System

let strToInt (str: string) =
    try
        let res = Convert.ToInt32 str
        Some res
    with
    | _ -> None
```



这里也可以换一种转换为int类型的方法，如下



```f#
let strToInt str =
    let b, i = Int32.TryParse str
    match b, i with
    | false, _ -> None
    | true, _ -> Some i
```



表示工作流的builder类写成如下



```f#
type YourWorkflowBuilder() =

    member this.Bind(x, f) =
        match x with
        | None -> None
        | Some a -> f a
    member this.Return(x) = 
            Some x   
``` 



最后实例化这个类

`let yourWorkflow = new YourWorkflowBuilder()`

运行测试代码，结果为

```f#
val good : int option = Some 17
val bad : int option = None
```

### 第二部分——创建一个 **bind** 函数


通过第一部分后，增加两个函数

```f#
let strAdd str i = ???
let (>>=) m f = ???
```

然后用上面的两个函数，可以将代码写成如下形式

```f#
let good = strToInt "1" >>= strAdd "2" >>= strAdd "3"
let bad = strToInt "1" >>= strAdd "xyz" >>= strAdd "3"  
```

解析：

首先很容易写出>>=运算符的定义

```f#
let (>>=) m f = Option.bind f m
```

然后，由第一部分可知， **strToInt**  函数返回结果类型为  **int option** ，
通过  **>>=**  运算符传给 **strAdd str** 函数（柯里化），
而  **>>=**  运算符内部的 **bind** 函数会对这个 **int option** 去包装化为 **int** 类型，
然后将这个 **int** 类型参数传给 **strAdd str** 函数（参见 **Option.bind** 函数的定义），
故可知 **strAdd** 函数签名为

```f#
strAdd: str:string -> i: int -> int option
```

尝试写出strAdd的函数定义

```f#
let strAdd str i =
    match strToInt str with
    | None -> None
    | Some a -> Some (a + i)
```

最后运行上面的测试代码，结果为

```f#
val good : int option = Some 6
val bad : int option = None
```

## 总结

 **Computation expressions**  是  **continuation passing** （后继传递）的语法糖，隐藏了链接逻辑。

 **bind** 是关键函数，用来连接前一步的输出到下一步的输入。

符号  **>>=**  是将  **bind**  写成中缀操作符的标准方式


# [Computation expressions and wrapper types](https://www.cnblogs.com/sjjsxl/p/4985557.html)

原文地址：[http://fsharpforfunandprofit.com/posts/computation-expressions-wrapper-types/](http://fsharpforfunandprofit.com/posts/computation-expressions-wrapper-types/)

在上一篇中，我们介绍了“ **maybe** ”工作流，让我们隐藏了写链接和可选类型的繁杂代码。

典型的“ **maybe** ”工作流大概类似



```f#
let result = 
    maybe 
        {
        let! anInt = expression of Option<int>
        let! anInt2 = expression of Option<int>
        return anInt + anInt2 
        }
```



这里有几个点奇怪的行为：

*   在 **let** ！行，等号后边的表达式是一个 **int option** ，但是等号左边的却是一个 **int** 。 **let** ！在将 **option** 绑定到左边的值之前已经对 **option** 去包装（ **unwrapped** ）
*   在 **return** 行，则进行相反的动作。被返回的表达式是一个 **int** ，但是整个“ **maybe** ”工作流的值是一个 **int option** 。也就是说， **return**  将原始的 **int** 值包装（ **wrapped** ）成一个 **option** 

在这一篇中，我们将继续这样的观察，并且将看到 **computation expression** 的一个主要用途：隐式的去包装（ **unwrapped** ）和复包装（ **rewrapped** ）一个值，这个值存储在某种包装类型中。

## 另一个例子

我们访问一个数据库，并想将结果放到一个 **Success/Error** 的联合类型中，如下

```f#
type DbResult<'a> = 
    | Success of 'a
    | Error of string
```

然后在访问数据库的方法中运用这个类型。以下是一些简单的例子演示如何使用 **DbResult** 类型


```f#

let getCustomerId name =
    if (name = "") 
    then Error "getCustomerId failed"
    else Success "Cust42"

let getLastOrderForCustomer custId =
    if (custId = "") 
    then Error "getLastOrderForCustomer failed"
    else Success "Order123"

let getLastProductForOrder orderId =
    if (orderId  = "") 
    then Error "getLastProductForOrder failed"
    else Success "Product456"
```



现在我们想将这些方法调用链接起来。

显式的方法如下，可以看到，每一步都需要进行模式匹配



```f#
let product = 
    let r1 = getCustomerId "Alice"
    match r1 with 
    | Error _ -> r1
    | Success custId ->
        let r2 = getLastOrderForCustomer custId 
        match r2 with 
        | Error _ -> r2
        | Success orderId ->
            let r3 = getLastProductForOrder orderId 
            match r3 with 
            | Error _ -> r3
            | Success productId ->
                printfn "Product is %s" productId
                r3
```



非常丑陋的代码。使用 **computation expression** 则可以拯救我们。



```f#
type DbResultBuilder() =

    member this.Bind(m, f) = 
        match m with
        | Error _ -> m
        | Success a -> 
            printfn "\tSuccessful: %s" a
            f a

    member this.Return(x) = 
        Success x

let dbresult = new DbResultBuilder()
```



有了这个类型的帮助，我们可以专注于整体结构而不用考虑一些细节，从而让代码简洁



```f#
let product' = 
    dbresult {
        let! custId = getCustomerId "Alice"
        let! orderId = getLastOrderForCustomer custId
        let! productId = getLastProductForOrder orderId 
        printfn "Product is %s" productId
        return productId
        }
printfn "%A" product'
```



如果出现错误，这个工作流会漂亮地捕获错误，并告诉我们错误发生的地方，例如



```f#
let product'' = 
    dbresult {
        let! custId = getCustomerId "Alice"
        let! orderId = getLastOrderForCustomer "" // error!
        let! productId = getLastProductForOrder orderId 
        printfn "Product is %s" productId
        return productId
        }
printfn "%A" product''
```



## 工作流中包装类型的角色

现在我们已经看到两个工作流了（ **maybe** 工作流和 **dbresult** 工作流），每个工作流都有自己的包装类型（ **Option\<T> ** 和 ** DbResult\<T>** ）。

这两个工作流并非有什么特别不同的。事实上，每个 **computation expression** 必须有相应的包装类型，而这个包装类型的设计通常与我们想要管理的工作流相关。

上面的例子中 **DbResult** 类型不仅仅是一个为了能返回值的简单类型，而是在工作流中扮演着关键的角色：存储工作流的当前状态（错误信息或成功时的结果信息）。通过利用这个 **DbResult** 类型的不同 **case ** （ ** Success** 或者是 **Error** ）， **dbresult** 工作流可以为我们做控制管理，并可以在后台执行一些信息（如打印信息）从而让我们专注于大局。

## 绑定和返回包装类型

再次复习一下 **Bind ** 和 ** Return** 方法的定义。

 **Return** 的签名[as documented on MSDN](http://msdn.microsoft.com/en-us/library/dd233182.aspx)如下，可以看到，对某种类型  **T**  ， **Return** 方法仅仅包装这个类型。

```f#
member Return : 'T -> M<'T>
```

说明：在签名中，包装类型常被称为  **M**  ，故 **M\<int>**  是应用到 **int** 的包装类型， **M\<string>**  是应用到 **string** 的包装类型，以此类推。

我们已经见过两个使用 **Return** 方法的例子了。 **maybe** 工作流返回一个 **Some** ，它是一个 **option** 类型， **dbresult** 工作流返回一个 **Sucess** ，它是 **DbResult** 类型。


```f#

// return for the maybe workflow
member this.Return(x) = 
    Some x

// return for the dbresult workflow
member this.Return(x) = 
    Success x
```



来看 **Bind** 的签名

```f#
member Bind : M<'T> * ('T -> M<'U>) -> M<'U>
```

 **Bind** 的输入参数为一个元组 **M\<'T>*（'T -> M\<'U>）** ，返回 **M\<'U>** ，即应用到类型U的包装类型。

其中元组有两部分

*    **M\<'T>**  是类型T的包装类型
*    **（'T -> M<'U>）**  是一个函数，以一个未包装的类型  **T**  作为输入参数，输出类型为应用到类型   **U**   上的包装类型

或者说， **Bind** 函数做的事情为：

*   将一个包装类型参数作为输入
*   将输入参数( **M<'T>** )去包装化为一个值(类型为   **T**   )，并对这个值做一些后台逻辑(自定义代码)。
*   应用函数到这个未包装的值（   **T**   ）上，并产生一个新的包装类型值（ **M<'U>** ）
*   即使没有应用这个函数， **Bind** 也必须返回一个类型U的包装类型（ **M<'U>** ）（参考前面安全除法中的除法出错的情况，此时没有应用 **continuation** 函数，返回的是 **None** ）

基于以上的理解，我们给出 **Bind** 的方法代码



```f#
// return for the maybe workflow
member this.Bind(m,f) = 
   match m with
   | None -> None
   | Some x -> f x

// return for the dbresult workflow
member this.Bind(m, f) = 
    match m with
    | Error _ -> m
    | Success x -> 
        printfn "\tSuccessful: %s" x
        f x
```



在此，确保你已经懂得了 **Bind** 方法所做的事情。

最后，给出一张图来帮助理解

![图片](http://fsharpforfunandprofit.com/assets/img/bind.png)

*   对 **Bind** 方法来说，从一个包装类型值开始（图中   **m**   ），将它去包装为一个类型   **T**   的原始值，然后（可能）应用函数   **f**    到这个值上，并获得一个类型   **U**   的包装类型
*   对 **Return** 方法来说，从一个值（图中   **x**   ）开始，简单的包装它并返回之。

## 类型包装器是泛型

注意到所有函数使用泛型类型（ **T** 和 **U** ）而不是包装类型，并且自始至终都如此。例如，不能阻止 **maybe** 的Bind函数（中的   **f**    函数）以一个 **int** 作为输入并返回一个 **Option<string>** ，或者以一个 **string** 为输入而返回一个 **Option<bool>** ，唯一的要求是总是返回一个可选类型 **Option<something>** 。

为了更好的理解，我们再看上面的例子，但比起到处使用 **string** ，我们将为客户 **id** ，订单 **id** 和产品 **id** 创建专有类型，这意味着每一步将使用不同的类型。

先给出类型定义



```f#
type DbResult<'a> = 
    | Success of 'a
    | Error of string

type CustomerId =  CustomerId of string
type OrderId =  OrderId of int
type ProductId =  ProductId of string
```



代码几乎相同，除了 **Success** 行改用了新类型。


```f#

let getCustomerId name =
    if (name = "") 
    then Error "getCustomerId failed"
    else Success (CustomerId "Cust42")

let getLastOrderForCustomer (CustomerId custId) =
    if (custId = "") 
    then Error "getLastOrderForCustomer failed"
    else Success (OrderId 123)

let getLastProductForOrder (OrderId orderId) =
    if (orderId  = 0) 
    then Error "getLastProductForOrder failed"
    else Success (ProductId "Product456")
```



应用以上函数，则代码变为



```f#
let product = 
    let r1 = getCustomerId "Alice"
    match r1 with 
    | Error e -> Error e
    | Success custId ->
        let r2 = getLastOrderForCustomer custId 
        match r2 with 
        | Error e -> Error e
        | Success orderId ->
            let r3 = getLastProductForOrder orderId 
            match r3 with 
            | Error e -> Error e
            | Success productId ->
                printfn "Product is %A" productId
                r3

```


从以上代码可以看出，我们可以预见即将写出来的 **Bind** 函数中的第一个
**continuation** 函数   **f**    的输入参数类型为 **string** 
（即 **“Alice”** ），输出类型为  **CustomerId option**  ，
而第二个 **continuation** 函数  **f**   的输入参数类型为 **CustomerId** ，
与前一个  **f**  函数的输出类型匹配。故可以知道， **Bind** 函数的输入参数类型为  **T** ，
输出类型为 **M\<U>** ，只要 **continuation** 中下一个函数的输入参数类型为  **U**  就行。

有几点变化值得讨论一下：

首先，底部的 **printfn** 使用 **"%A"** 格式化器而不是 **"%s"** 。这是因为 **ProductId** 类型是联合类型。

更为细致地，错误行的代码看起来似乎是不必要的。为啥要写 ** | Error e -> Error e **  ？原因是  **->**  左边的错误类型与类型 **DbResult<CustomerId>** 或者 **DbResult<OrderId>** 匹配，但是右边的错误类型必须为 **DbResult<ProductId>** 。故即使两个 **Error** 看起来一样，但其实它们是不同的类型

下一步，是builder类型，


```f#

type DbResultBuilder() =

    member this.Bind(m, f) = 
        match m with
        | Error e -> Error e
        | Success a -> 
            printfn "\tSuccessful: %A" a
            f a

    member this.Return(x) = 
        Success x

let dbresult = new DbResultBuilder()
```



最后我们使用工作流


```f#

let product' = 
    dbresult {
        let! custId = getCustomerId "Alice"
        let! orderId = getLastOrderForCustomer custId
        let! productId = getLastProductForOrder orderId 
        printfn "Product is %A" productId
        return productId
        }
printfn "%A" product'
```



这一次，每一行的返回值都不同类型（ **DbResult<CustomerId>**, **DbResult<OrderId>** 等），
但是因为他们有相同的包装类 **DbResult** ，故可以如期望一样正常工作。

最后，给出工作流的一个出错的情况的示例



```f#
let product'' = 
    dbresult {
        let! custId = getCustomerId "Alice"
        let! orderId = getLastOrderForCustomer (CustomerId "") //error
        let! productId = getLastProductForOrder orderId 
        printfn "Product is %A" productId
        return productId
        }
printfn "%A" product''
```



## 组合computation expression


我们已经知道每个 **computation expression** 都必须要有相应的包装类型。这个包装类型用在 **Bind ** 和 ** Return** 中，可以有一个好处：

*    **Return** 的输出可以传送给 **Bind** 作为输入

或者说，因为工作流返回一个包装类型，并且 **let** ！消费一个包装类型，你可以将一个“子”工作流放到 **let** ！表达式的右边。

例如，有一个工作流为 **myworkflow** ，然后可以写如下代码



```f#
let subworkflow1 = myworkflow { return 42 }
let subworkflow2 = myworkflow { return 43 }

let aWrappedValue = 
    myworkflow {
        let! unwrappedValue1 = subworkflow1
        let! unwrappedValue2 = subworkflow2
        return unwrappedValue1 + unwrappedValue2
        }
```



或者以行内的形式运用这个工作流



```f#
let aWrappedValue = 
    myworkflow {
        let! unwrappedValue1 = myworkflow {
            let! x = myworkflow { return 1 }
            return x
            }
        let! unwrappedValue2 = myworkflow {
            let! y = myworkflow { return 2 }
            return y
            }
        return unwrappedValue1 + unwrappedValue2
        }
```



如果已经用过 **async** 工作流，你可能已经实现过这样的处理，因为 **async** 工作流通常包含其他 **asyncs** 



```f#
let a = 
    async {
        let! x = doAsyncThing  // nested workflow
        let! y = doNextAsyncThing x // nested workflow
        return x + y
    }
```



## 介绍“ReturnFrom”

我们已经使用 **return** 作为一种包装一个类型并返回这个包装类型的简单方法。

但是，有时候我们的函数已经返回了一个包装类型，我们想直接返回它， **return** 不适合做这个事情，因为它要求一个非包装类型作为输入。

解决方法是采用 **return** ！，它采用一个包装类型作为输入并返回这个包装类型。

“builder”类中相应的方法称为 **ReturnFrom** 。实现方法通常仅仅是返回这个包装类型（当然，你可以增加额外的代码来实现一些后台逻辑）。

以下是“ **maybe** ”工作流的变体，



```f#
type MaybeBuilder() =
    member this.Bind(m, f) = Option.bind f m
    member this.Return(x) = 
        printfn "Wrapping a raw value into an option"
        Some x
    member this.ReturnFrom(m) = 
        printfn "Returning an option directly"
        m

let maybe = new MaybeBuilder()
```



用法如下，同return比较


```f#

// return an int
maybe { return 1  }

// return an Option
maybe { return! (Some 2)  }
```



一个更实际的例子


```f#

// using return
maybe 
    {
    let! x = 12 |> divideBy 3
    let! y = x |> divideBy 2
    return y  // return an int
    }    

// using return!    
maybe 
    {
    let! x = 12 |> divideBy 3
    return! x |> divideBy 2  // return an Option
    }
```



## 总结

本篇文章介绍了包装类型以及包装类型与 **Bind ** 、 ** Return ** 和 ** ReturnFrom** 方法的关系。

下一篇，我们继续讨论包装类型，包括使用列表作为包装类型。


# [More on wrapper types](https://www.cnblogs.com/sjjsxl/p/4989248.html)

原文地址：[http://fsharpforfunandprofit.com/posts/computation-expressions-wrapper-types-part2/](http://fsharpforfunandprofit.com/posts/computation-expressions-wrapper-types-part2/)

上一篇中，我们说明了包装类型的概念以及与 **computation expression** 的关系。在这一篇中，我们将介绍什么类型是合适的包装类型。

什么样的类型可以是包装类型？

每个 **computation expression** 必须要有相应的包装类型，那么什么样的类型可以作为包装类型呢？对包装类型是否有特殊的限制？

有一个通用的原则为：

*   任何带有泛型参数的类型均可以用作包装类型

例如，你可以使用 **Option\<T>, DbResult\<T>** 等作为包装类型。也可以使用限制了类型参数的包装类型，如 **Vector\<int>** 。

但是对于其他泛型类型如 **List\<T>** 或者 **IEnumerable\<T>** 如何呢？事实上，它们也可以被用作包装类型，我们一会就可以看到。

## 非泛型包装类型是否可行？

是否可以使用一个不带泛型参数的包装类型？

例如，在以前的例子中我们见过一个 **string** 上的加法，如"1" + "2"。我们不能聪明地将 **string** 看成一个 **int** 的包装类型吗？这很酷，是吧

我们来试一试，可是借助Bind和Return的签名帮助我们来实现。

*   Bind函数输入为一个元组。元组的第一部分是一个包装类型（这个例子中是string），第二部分是一个函数，这个函数以一个非包装类型作为输入，并将输入转变为一个包装类型（ **T -> M\<U>** ）。这个例子中，函数签名是int -> string。
*   Return以一个非包装类型作为输入（这个例子中为int）并将输入转变为一个包装类型，这个例子中，Return签名为int -> string。

以上函数签名如何指导实现过程？

“包装”函数的实现，int -> string，是很简单的，就是int类型的“toString”方法。

Bind函数必须去包装一个string为一个int，然后将这个int传入continuation函数  **f**   ，我们可以使用int.Parse函数实现这个去包装操作。

如果Bind函数无法对一个string去包装，因为这个string不是一个有效数字，此时如何处理？这种情况下，绑定函数必须仍然返回一个包装类型（这里是string），所以我们可以只返回一个string如“error”。

builder类的实现如下



```f#
type StringIntBuilder() =

    member this.Bind(m, f) = 
        let b,i = System.Int32.TryParse(m)
        match b,i with
        | false,_ -> "error"
        | true,i -> f i

    member this.Return(x) = 
        sprintf "%i" x

let stringint = new StringIntBuilder()
```



现在我们可以尝试使用



```f#
let good = 
    stringint {
        let! i = "42"
        let! j = "43"
        return i+j
        }
printfn "good=%s" good

```


如果有一个string无效，那么会发生什么



```f#
let bad = 
    stringint {
        let! i = "42"
        let! j = "xxx"
        return i+j
        }
printfn "bad=%s" bad
```



看起来不错——在我们的工作流中，将strings看成ints。

但是等下，有问题。

我们给这个工作流一个输入，对输入进行去包装（使用 **let** ！），然后立即复包装它（使用return），这其中没有做其他任何事情。会发生什么情况？



```f#
let g1 = "99"
let g2 = stringint {
            let! i = g1
            return i
            }
printfn "g1=%s g2=%s" g1 g2
```



以上这段代码没有问题。输入g1和输出g2是相同的值，如我们所期望一样。

但是如果是字符串转换为int时发生错误的情况呢？



```f#
let b1 = "xxx"
let b2 = stringint {
            let! i = b1
            return i
            }
printfn "b1=%s b2=%s" b1 b2
```



这种情况下，我们得到一个跟期望不同的行为。输入b1和输出b2不是相同的值。我们引入了不一致问题。

这在实际中会是一个问题吗？我不清楚，但是我将避免它，使用一个不同的方法，如options，在所有情况都是一致的。

## 工作流使用包装类型的原则

有个问题，如下代码所示，这两段代码有什么不同，它们的行为是否不同？



```f#
// fragment before refactoring
myworkflow {
    let wrapped = // some wrapped value
    let! unwrapped = wrapped
    return unwrapped 
    } 
    
// refactored fragment 
myworkflow {
    let wrapped = // some wrapped value
    return! wrapped
    }
```



答案是否定的，即它们的行为不应该不同。唯一的不同是在第二个例子中，unwrapped值已经被重构了，直接返回wrapped值。

但是正如我们在前一小节中所见，如果不小心则会引入不一致问题。故，任何一种实现都应该遵循一些标准原则，总结如下：

 **原则1：如果以一个非包装类型值开始，然后包装它（使用return），然后去包装它（使用bind），那么总是可以回到初始的非包装类型值** 

这个原则以及下一个原则的关注点为：当包装和去包装值的时候不会丢失信息。

用代码表示这个原则如下



```f#
myworkflow {
    let originalUnwrapped = something
    
    // wrap it
    let wrapped = myworkflow { return originalUnwrapped }

    // unwrap it
    let! newUnwrapped = wrapped

    // assert they are the same
    assertEqual newUnwrapped originalUnwrapped 
    }
```



 **原则2： 如果以一个包装类型值开始，然后去包装这个值（使用bind），然后包装它（使用return），则总是可以回到初始的包装类型值。** 

这个原则跟上面的stringInt工作流一致。

用代码表示则如下



```f#
myworkflow {
    let originalWrapped = something

    let newWrapped = myworkflow { 

        // unwrap it
        let! unwrapped = originalWrapped
        
        // wrap it
        return unwrapped
        }
        
    // assert they are the same
    assertEqual newWrapped originalWrapped
    }
```



  **原则3：如果创建一个子工作流，那它必须产生与主工作流相同的结果，就好像是将逻辑嵌入到主工作流中。** 

这个原则要求正确组合。

用代码演示则如下



```f#
// inlined
let result1 = myworkflow { 
    let! x = originalWrapped
    let! y = f x  // some function on x
    return! g y   // some function on y
    }

// using a child workflow ("extraction" refactoring)
let result2 = myworkflow { 
    let! y = myworkflow { 
        let! x = originalWrapped
        return! f x // some function on x
        }
    return! g y     // some function on y
    }

// rule
assertEqual result1 result2
```



## 将“列表”作为包装类型

之前提过List\<T>或者IEnumerable\<T>可以作为包装类型，但是怎么实现呢？在包装类型和非包装类型之间没有一对一的对应关系。

这正是“包装类型”类比有一点点误导的地方。我们回想一下bind，bind是一种将一个表达式的输出与另一个表达式的输入联系起来的方法。

我们已经看到，bind函数去包装一个类型，然后将continuation函数  **f**   应用到这个去包装后的值上。但是没有任何规定说只能有一个未包装值。没有理由说我们不能依次应用continuation函数到一个list的每一项上。

也就是说，我们能够写一个bind，这个bind的输入参数为由一个列表以及一个continuation函数  **f**   组成的元组，且continuation函数  **f**   每次处理这个列表中的一个元素，如下

```f#
bind( [1;2;3], fun elem -> // expression using a single element )
```

有了这个概念后，我们可以将一些bind链接起来如下



```f#
let add = 
    bind( [1;2;3], fun elem1 -> 
    bind( [10;11;12], fun elem2 -> 
        elem1 + elem2
    ))
```



但是我们忽略了一些重要的东西。传入bind的continuation函数f 必须要符合某种函数签名，即有一个未包装类型作为输入参数，并产生一个包装类型的输出。

换句话说，continuation函数  **f**   产生的结果必须总是一个新列表(因为类型包装M必须相同，而这里用列表来包装类型)

```f#
bind( [1;2;3], fun elem -> // expression using a single element, returning a list )
```

这样，我们则必须将上面那个链接起来的代码写成如下形式，其中elem1+elem2的结果被放入一个列表中



```f#
let add = 
    bind( [1;2;3], fun elem1 -> 
    bind( [10;11;12], fun elem2 -> 
        [elem1 + elem2] // a list!
    ))
```



所以我们bind方法的逻辑类似这样

```f#
let bind(list,f) =
    // 1) for each element in list, apply f
    // 2) f will return a list (as required by its signature)
    // 3) the result is a list of lists
```

现在又已经导致另一个问题了。因为continuation函数  **f**   必须返回一个列表类型，而对作为输入参数的列表的每个元素应用函数  **f**  ，则产生一个“列表的列表”，“列表的列表”不好，我们需要将它们转成简单的一阶列表。

不过这已经很简单了，因为已经有一个模块函数能做到，即concat

故将以上相关代码放到一起，我们有



```f#
let bind(list,f) =
    list 
    |> List.map f 
    |> List.concat

let added = 
    bind( [1;2;3], fun elem1 -> 
    bind( [10;11;12], fun elem2 -> 
//       elem1 + elem2    // error. 
        [elem1 + elem2]   // correctly returns a list.
    ))
```



现在我们知道了bind工作机制，就能够自己创建一个“列表工作流”

*   Bind对传入的列表的每一个元素应用continuation函数  **f**  ，然后将“列表的列表”展平，得到一个一阶列表。List.collect就是一个能做到如此的库函数。
*   Return将未包装类型转为包装类型。这意味着将返回值包装成列表。



```f#
type ListWorkflowBuilder() =

    member this.Bind(list, f) = 
        list |> List.collect f 
    
    member this.Return(x) = 
        [x]

let listWorkflow = new ListWorkflowBuilder()





let added = 
    listWorkflow {
        let! i = [1;2;3]
        let! j = [10;11;12]
        return i+j
        }
printfn "added=%A" added

let multiplied = 
    listWorkflow {
        let! i = [1;2;3]
        let! j = [10;11;12]
        return i*j
        }
printfn "multiplied=%A" multiplied

```


结果显示第一个集合中的每个元素，其中第一个集合由第二个集合中的每个元素组成。

```f#
val added : int list = [11; 12; 13; 12; 13; 14; 13; 14; 15]
val multiplied : int list = [10; 11; 12; 20; 22; 24; 30; 33; 36]
```

非常奇妙，我们完全隐藏了列表枚举的逻辑，只暴露了工作流本身。

## “for”语法糖

如果将列表和序列特别对待，我们可以增加一个语法糖：用一个更自然的东西代替let！

用for..in..do表达式代替let！



```f#
// let version
let! i = [1;2;3] in [some expression]

// for..in..do version
for i in [1;2;3] do [some expression]
```



为了让F#编译器能做到这点，我们需要增加一个For方法到我们到build类。For方法与一般的Bind方法的实现相同，但是要求接收一个序列类型（Bind函数对包装类型则没有限制为序列类型）



```f#
type ListWorkflowBuilder() =

    member this.Bind(list, f) = 
        list |> List.collect f 
    
    member this.Return(x) = 
        [x]

    member this.For(list, f) = 
        this.Bind(list, f)

let listWorkflow = new ListWorkflowBuilder()
```



以下是使用方法



```f#
let multiplied = 
    listWorkflow {
        for i in [1;2;3] do
        for j in [10;11;12] do
        return i*j
        }
printfn "multiplied=%A" multiplied
```



## LINQ和“list工作流”

这个 for element in collection do 看起熟悉吗？它非常接近于LINQ的from element in collection...语法。事实上，LINQ使用基本相同的方法在后台实现将一个查询表达式如from element in collection... 转为实际的调用方法。

F#中，bind使用 形如 List.collect函数。LINQ中与List.collect等价的是 SelectMany扩展方法。如果知道SelectMany的工作原理，就可以实现相同的查询。参见Jon Skeet的博客 a [helpful blog post](http://msmvps.com/blogs/jon_skeet/archive/2010/12/27/reimplementing-linq-to-objects-part-9-selectmany.aspx) 

## “包装类型”本质

本篇我们已经见过很多包装类型了，并且已经说明每个computation expression必须有相对应的包装类型。但是，还记得一开始的那个logging例子吗？那个例子中没有包装类型，有let！在后台执行的逻辑，但是输入类型与输出类型相同，类型没有被改变。

简单来说，可以将任意类型看作是自身的包装类型，但是，也可以从一个 更深的层次理解这一点。

让我们回过头去考虑一下包装类型如List<T>到底是什么。

如果有一个类型如List\<T>，实际上这个类型不是一个真正的类型。List\<int>是真正的类型，List\<string>也是真正的类型，但是List\<T>本身是不完整的，它缺少一个能变成真正类型的参数。

一种方法是将List\<T>看成一个函数，而不是一个类型，它是类型的抽象世界的一个函数，而不是值的具体世界的一个函数，但是正如那些将一个值映射到另一个值的函数一样，List\<T>，其输入为类型（如int或者string），输出为其他类型（如List\<int>或List\<string>）。List\<T>跟其他函数一样，它有一个参数，即“类型参数”，.net开发者所谓的泛型在计算机科学就是“参数多态”。

一旦我们掌握了函数的概念，即，从一个类型产生另一个类型（称为”类型构造器“），就可以明白当说一个包装类型时，我们指的是一个类型构造器。

但是，如果包装类型仅仅是一个函数，它将一个类型映射到另一个类型，那么，可以确定将一个类型映射到同样的类型的函数也符合吗？嗯，没错。“identity”函数符合我们的定义，可以被用作computation expression的包装类型。

回到代码中来，我们可以定义一个“identity”工作流，它非常简单


```f#

type IdentityBuilder() =
    member this.Bind(m, f) = f m
    member this.Return(x) = x
    member this.ReturnFrom(x) = x

let identity = new IdentityBuilder()

let result = identity {
    let! x = 1
    let! y = 2
    return x + y
    } 

```


有了这些概念，我们可以知道先前讨论的logging的例子就是一个添加了打印log信息的“identity”工作流。

## 总结

本篇涵盖了很多主题，希望能对包装类型有个更清楚的认识。我们了解到如何在实际中使用包装类型。

总结一下本篇中的几个关键点：

*   computation expression的主要作用是去包装一个类型以及复包装。
*   可以很容易组合computation expression，因为Return的输出匹配Bind的输入，都是包装类型
*   每个computation expression必须有一个相关的包装类型
*   任何带有一个泛型参数的类型都可以被用作包装类型，即使是列表也是如此。
*   当创建工作流时，需要确保工作流的实现满足三个有关包装、去包装以及组合的原则。


# [Implementing a builder: Zero and Yield](https://www.cnblogs.com/sjjsxl/p/5005701.html)


原文地址：[http://fsharpforfunandprofit.com/posts/computation-expressions-builder-part1/](http://fsharpforfunandprofit.com/posts/computation-expressions-builder-part1/)

前面介绍了bind和continuation，以及包装类型的使用，现在我们已经准备讲述“builder”类型的一系列的方法了。

如果你看过[MSDN documentation](http://msdn.microsoft.com/en-us/library/dd233182.aspx)，你会发现“builder”类型的方法不仅仅是Bind和Return，还有其他方法如Delay和Zero，这两个方法是干啥用的？这正是本篇以及以后几篇讲述的主题。

## 计划

为了论证如何创建一个builder类，我们将创建一个自定义工作流，它使用所有可能的builder类方法。

但是比起从头开始，没有上下文就解释那些builder类方法是什么意思，我们不如采用自底向上的方式，从一个简单的工作流开始，并在需要解决某个问题或者错误的时候添加有关的方法，这样会更加自然些。在这个过程中，你将逐渐理解F#在细节上如何处理computation expression的。

对这个过程做一个提纲

1.  首先看一看，作为一个基本工作流，哪些方法是必需的。我们将介绍Zero，Yield，Combine和For
2.  然后，我们将讨论如何延迟执行代码，即，只在需要的时候计算值。我们介绍Delay和Run，然后看延迟计算的具体实现。
3.  最后，我们讨论剩下的方法：While，Using和异常处理。

## 开始前

在我们开始创建工作流之前，有几点说明

### computation expressions文档说明


也许你已经注意到，MSDN文档中关于computation expressions的说明是非常简陋的，尽管它并非不准确，但是仍然容易会误导初学者。例如，builder类方法的签名比看上去更加灵活，这有助于实现一些特性（features），你在工作中如果不随时查阅文档说明，那这些特性则很不明显。后面我们会给出一个例子说明。

如果你需要更详细的文档说明，那么我将推荐两个源。如果想更为详细地纵览computation expressions背后的概念，有一个很好的资源，是由Tomas Petricek和Don Syme写的论文["The F# Expression Zoo"。](http://tomasp.net/academic/papers/computation-zoo/computation-zoo.pdf)如果需要最新的准确的科技文档，可以阅读[F# language specification](http://research.microsoft.com/en-us/um/cambridge/projects/fsharp/manual/spec.pdf)，它有一章节是关于computation expressions的。

### 包装类型和未包装类型


当想去理解文档中的函数签名，记住一点，我们通常把未包装类型记为'T，包装类型记为M<'T>。这样以后当看到Return方法的签名为'T -> M<'T>，它表示Return方法有一个未包装类型的输入参数，输出参数是要给包装类型。

跟这个系列的前面几篇文章相同，我将继续使用“未包装”和“包装”来描述这些类型之间的关系，但是我们接下去的讨论中可能会稍作改变，使用其他术语，如“computation type”代替“wrapped type”。我希望当碰到这个称呼改变时，我们对这个作改变的理由是清楚的并能理解。

当然，在我的例子中，我将尽量让事情简单，比如使用

```f#
let! x = ...wrapped type value...
```

但是，这其实是一个过分简化的表达式，也可以准确的说，“x”可以是任意模式而不仅是一个值，“wrapped type value”可以是一个表达式，这个表达式能计算得到一个包装类型。MSDN文档使用这种更加精确的方法，它在定义中使用“模式”和“表达式”，如

```f#
let! pattern = expr in cexpr
```

以下是maybe工作流中使用模式和表达式的一些例子，其中，Option是包装类型，



```f#
// let! pattern = expr in cexpr
maybe {
    let! x,y = Some(1,2) 
    let! head::tail = Some( [1;2;3] )
    // etc
    }
```



之前说过，我将继续使用这种非常简单的例子，不会对已经很复杂的主题再添加更多复杂的东西。

### 实现builder类（或者不是builder类）的特殊方法


MSDN文档说明了，每个特殊操作（如for..in，或yield）是如何被转换成builder类中的一个或多个方法调用的。

虽然没有一对一的对应关系（特殊操作与builder类方法），但是通常地，为了支持特殊操作的语法，你必须实现builder类中的一个对应的方法，否则编译器无法通过编译。

另一方面，你不必实现每个方法，如果不需要支持某个语法的话。例如，通过实现Bind和Return两个方法，我们就已经实现了maybe工作流。如果我们不需要使用Delay，Use等等，我们就不必实现这些方法。

为了能够看到如果不实现必需的方法，将会发生什么，让我们尝试在maybe工作流中石油for..in..do语法，如下

```f#
maybe { for i in [1;2;3] do i }
```

我们将会获得一个编译错误

This control construct may only be used if the computation expression builder defines a 'For' method

有时候会获得隐晦的错误，除非你知道后台进行了什么操作。例如，如果忘记在工作流中加Return操作，如下

```f#
maybe { 1 }
```

那你会获得编辑错误

This control construct may only be used if the computation expression builder defines a 'Zero' method

你可能会问：Zero方法是什么？为什么需要这个方法？答案下面会给出。

### 带"!"和不带"!"的操作


显然，很多特殊操作是成对出现的，即带"!"和不带"!"符号。例如，let 和 let！ （发音为 let-bang），return 和 return！，yield和yield！等等。

区别其实很简单。当实现不带"!"的操作时，右边总是非包装类型，而带"!"的操作右边总是包装类型。

例如，使用maybe工作流，其中Option是包装类型，我们可以比较下面代码的语法的不同



```f#
let x = 1           // 1 is an "unwrapped" type
let! x = (Some 1)   // Some 1 is a "wrapped" type
return 1            // 1 is an "unwrapped" type
return! (Some 1)    // Some 1 is a "wrapped" type
yield 1             // 1 is an "unwrapped" type
yield! (Some 1)     // Some 1 is a "wrapped" type
```



带"!"的对工作流组合非常重要，因为包装类型可以是另一个相同类型的computation expression的结果


```f#

let! x = maybe {...)       // "maybe" returns a "wrapped" type

// bind another workflow of the same type using let!
let! aMaybe = maybe {...)  // create a "wrapped" type
return aMaybe             // return it

// bind two child asyncs inside a parent async using let!
let processUri uri = async {
    let! html = webClient.AsyncDownloadString(uri)
    let! links = extractLinks html
    ... etc ...
    }
```



以上代码中，webClinet.AsyncDownloadString(uri) 与 extractLinks html 结果为一个包装类型，故可以放在let！操作的右边。

## 深入——创建一个工作流的最小实现

让我们开始吧！我们开始创建一个maybe工作流的最小版本（我们将重命名它为“trace”），实现上面说过的每一个方法，可以让我们知道有了这些实现会发生什么。以后，我们就使用这个工作流作为我们的试验台。

给出trace工作流的第一个版本


```f#

type TraceBuilder() =
    member this.Bind(m, f) = 
        match m with 
        | None -> 
            printfn "Binding with None. Exiting."
        | Some a -> 
            printfn "Binding with Some(%A). Continuing" a
        Option.bind f m

    member this.Return(x) = 
        printfn "Returning a unwrapped %A as an option" x
        Some x

    member this.ReturnFrom(m) = 
        printfn "Returning an option (%A) directly" m
        m

// make an instance of the workflow 
let trace = new TraceBuilder()
```



这里的代码没有什么新的内容，都是之前见过的。

现在让我们运行一些示例代码



```f#
trace { 
    return 1
    } |> printfn "Result 1: %A" 

trace { 
    return! Some 2
    } |> printfn "Result 2: %A" 

trace { 
    let! x = Some 1
    let! y = Some 2
    return x + y
    } |> printfn "Result 3: %A" 

trace { 
    let! x = None
    let! y = Some 1
    return x + y
    } |> printfn "Result 4: %A" 
```



一切都应该如期工作，尤其，你应该能够看到第四个例子中使用了None，这导致接下来的两行`（let! y = ... return x+y）`代码被跳过执行，整个表达式的结果是None。

## 介绍"do!"

我们的表达式支持let！，但是do！是什么？

在正常F#中，do就像let，除了do表达式不返回值（即，返回unit值）。

在computation expression中，do！也非常类似let！，传入一个包装类型到Bind方法中，但是do！的结果值是unit，所以传给Bind方法的是一个unit的包装类型。

以下是使用trace工作流的简单例子



```f#
trace { 
    do! Some (printfn "...expression that returns unit")
    do! Some (printfn "...another expression that returns unit")
    let! x = Some (1)
    return x
    } |> printfn "Result from do: %A"

```


输出结果为



```f#
...expression that returns unit
Binding with Some(<null>). Continuing
...another expression that returns unit
Binding with Some(<null>). Continuing
Binding with Some(1). Continuing
Returning a unwrapped 1 as an option
Result from do: Some 1
```



## 介绍"Zero"

最小的工作流会是什么样子？如果什么都不做，如下代码

```f#
trace { 
    } |> printfn "Result for empty: %A" 
```

我们会获得一个错误

This value is not a function and cannot be applied

想一下，在工作流中没有任何东西，这也没什么意义，毕竟本来的目的就是链接表达式的。

接着，如果在工作流中添加一个简单的不带let！或者return的表达式呢？

```f#
trace { 
    printfn "hello world"
    } |> printfn "Result for simple expression: %A"
```

于是，将会获得如下错误

This control construct may only be used if the computation expression builder defines a 'Zero' method

所以你知道了什么需要Zero方法了吧？答案就是在这种情况下，没有显式返回任何东西，而computation expression整体必须返回一个包装值。那应该返回什么呢？

事实上，只要没有显式给出computation expression的返回值，就会发生上面这种情况，比如你写if..then表达式但少了else从句，就会发生同样的状况。

```f#
trace { 
    if false then return 1
    } |> printfn "Result for if without else: %A" 
```

在正常的F#代码中，“if..then"语句不带“else”的会产生一个unit类型的结果，但是在computation expression中，返回值必须是一个包装类型，而编译器不知道这个包装值是什么。

办法就是告诉编译器该使用什么——这就是Zero方法的目的。

### Zero应该使用什么值？


那Zero应该使用什么值呢？这依赖于你创建的是什么工作流。

以下几点说明也许会有帮助：

*    工作流是否有“成功”或者“失败”的概念？如果是，则为Zero使用“失败”的结果值。例如，在我们的trace工作流中，我们使用None表示失败，所以我们可以使用None作为Zero的值
*   工作流是否是顺序执行？也就是，在工作流中，一步一步的执行，包括一些在后台的处理。在一般的F#代码中，有显式返回的表达式可能会计算得到unit，所以这里类似处理，即Zero是unit的包装版本。例如，在一个option-based 工作流中，我们可能使用Some()来表示Zero（顺便一提，这跟Return()是相同的）。
*   工作流是否主要关心数据结构的操作？如果是，Zero应该是“空”数据结构。例如，在一个“list builder”工作流中，我们将使用空列表作为Zero值。
*   在组合包装类型时Zero值也扮演着重要的作用。

### 一个Zero的实现


让我们扩展一下我们的测试代码，使用一个返回None的Zero方法，如下



```f#
type TraceBuilder() =
    member this.Zero () =
        printfn "Zero"
        None
let trace = new TraceBuilder()

trace {
        printfn "hello world"
        } |> printfn "Result for simple expression: %A"

trace {
        if false then return 1
        | |> printfn "Return for if without else clause: %A"
```



通过以上测试代码我们很清楚地看到，Zero在后台被默默的调用。整个表达式的返回值为None。注意：None可能被打印成<null>。

### 是否总是需要Zero？


记住，不要求必须有Zero，除非它在工作流的上下文中有意义。例如，seq不允许有Zero，而async则可以

```f#
let s = seq {printfn "zero" }    // Error
let a = async {printfn "zero" }  // OK
```

## 介绍“Yield”


C#中有“yield”语句，其作用是提前返回一个可枚举类型，以后通过迭代器在需要用到可枚举类型的元素时才获取元素值。

F# computation expressions中也有一个“yield”语句，它是干嘛的呢？

让我们尝试一下

```f#
trace { 
    yield 1
    } |> printfn "Result for yield: %A"
```

运行这段代码会获得一个错误

This control construct may only be used if the computation expression builder defines a 'Yield' method

这里不奇怪。那“yield”方法的实现是什么样的呢？MSDN文档给出了它的签名'T -> M<'T>，跟Return方法签名相同，即包装一个非包装类型。

所以，我们类似Return方法来实现Yield方法，并测试这个表达式



```f#
type TraceBuilder() =
    // other members as before

    member this.Yield(x) = 
        printfn "Yield an unwrapped %A as an option" x
        Some x

// make a new instance        
let trace = new TraceBuilder()

// test
trace { 
    yield 1
    } |> printfn "Result for yield: %A" 
```



此时，这个工作流可以工作，看起来yield可以是return的替代品。

当然还有一个YieldFrom方法，它对应ReturnFrom方法。YieldFrom的行为类似，产生一个包装类型

那下面将YieldFrom加入builder类方法



```f#
type TraceBuilder() =
    // other members as before

    member this.YieldFrom(m) = 
        printfn "Yield an option (%A) directly" m
        m

// make a new instance        
let trace = new TraceBuilder()

// test
trace { 
    yield! Some 1
    } |> printfn "Result for yield!: %A" 

```


此时，可能会想：如果return和yield几乎相同，那为何要存在这两个关键字呢？答案是，实现其中一个方法可以让我们使用某个语法，而实现另一个则不能使用这个语法。例如，seq表达式允许yield但是不允许return，而async允许return但不允许yield，如下面代码片段



```f#
let s = seq {yield 1}    // OK
let s = seq {return 1}   // error

let a = async {return 1} // OK
let a = async {yield 1}  // error
```



事实上，你可以实现让return与yield方法之间存在一些差别，那样就能达到某种目的，比如使用return停止（return）后面的computation expression的计算，而yield则不会。

更一般地，yield应该用在sequence/enumeration的场景，而return通常一次作用在一个表达式。（我们将在下一篇中看到如何多次使用yield。）

## 复习“For”

我们在上一篇中讨论了for..in..do语法，现在再来回顾一下“list builder”并向其中添加一些其他的方法。我们已经知道如何对一个列表定义Bind和Return方法了，故这里只要实现其他的方法定义。

Zero方法只返回一个空列表。

Yield方法实现与Return相同。

For方法实现与Bind相同。



```f#
type ListBuilder() =
    member this.Bind(m, f) = 
        m |> List.collect f

    member this.Zero() = 
        printfn "Zero"
        []

    member this.Return(x) = 
        printfn "Return an unwrapped %A as a list" x
        [x]

    member this.Yield(x) = 
        printfn "Yield an unwrapped %A as a list" x
        [x]
        
    member this.For(m,f) =
        printfn "For %A" m
        this.Bind(m,f)

// make an instance of the workflow                
let listbuilder = new ListBuilder()
```



下面是使用let！的代码



```f#
listbuilder { 
    let! x = [1..3]
    let! y = [10;20;30]
    return x + y
    } |> printfn "Result: %A"
```



下面是使用for的代码


```f#

listbuilder { 
    for x in [1..3] do
    for y in [10;20;30] do
    return x + y
    } |> printfn "Result: %A" 
```



可以看到这两段代码的输出结果相同

## 总结

本篇我们看到了对一个简单的computation expression，如何实现它的基本的方法。

重申几点：

对简单的表达式来说，不必实现所有的方法。

*   带“!”的语句，表达式右边是包装类型
*   不带“!”的表达式右边是非包装类型
*   如果想让一个工作流不显式返回值，则必须实现Zero
*   Yield与Return基本相同，但是Yield应该在sequence/enumeration语义中使用
*   在一些简单的场景中For与Bind基本相同

下一篇，我们将看到当组合多个值时会发生什么。

# [Implementing a builder: Combine](https://www.cnblogs.com/sjjsxl/p/5011520.html)


原文地址：[点击这里](http://fsharpforfunandprofit.com/posts/computation-expressions-builder-part2/)

本篇我们继续讨论从一个使用Combine方法的computation expression中返回多值。

## 前面的故事

到现在为止，我们的表达式建造（builder）类如下



```f#
type TraceBuilder() =
    member this.Bind(m, f) = 
        match m with 
        | None -> 
            printfn "Binding with None. Exiting."
        | Some a -> 
            printfn "Binding with Some(%A). Continuing" a
        Option.bind f m

    member this.Return(x) = 
        printfn "Returning a unwrapped %A as an option" x
        Some x

    member this.ReturnFrom(m) = 
        printfn "Returning an option (%A) directly" m
        m

    member this.Zero() = 
        printfn "Zero"
        None

    member this.Yield(x) = 
        printfn "Yield an unwrapped %A as an option" x
        Some x

    member this.YieldFrom(m) = 
        printfn "Yield an option (%A) directly" m
        m
        
// make an instance of the workflow                
let trace = new TraceBuilder()
```



这个类到现在工作正常。但是，我们即将看到一个问题

## 两个‘yield’带来的问题

之前，我们看到yield可以像return一样返回值。

通常来说，yield不会只使用一次，而是使用多次，以便在一个过程中的不同阶段返回多个值，如枚举（enumeration）。如下代码所示

```f#
trace { 
    yield 1
    yield 2
    } |> printfn "Result for yield then yield: %A" 
```

但是运行这段代码，我们获得一个错误

This control construct may only be used if the computation expression builder defines a 'Combine' method.

并且如果你使用return来代替yield，你会获得同样的错误
```f#

trace { 
    return 1
    return 2
    } |> printfn "Result for return then return: %A" 
```

在其他上下文中，也同样会有这个错误，比如我们想在做某事后返回一个值，如下代码

```f#
trace { 
    if true then printfn "hello" 
    return 1
    } |> printfn "Result for if then return: %A" 
```

我们会获得同样的错误。

## 理解这个问题

那这里该怎么办呢？

为了帮助理解，我们回到computation expression的后台视角，我们能看到return和yield是一系列计算的最后一步，就比如
```f#

Bind(1,fun x -> 
   Bind(2,fun y -> 
     Bind(x + y,fun z -> 
        Return(z)  // or Yield
```

可以将return（或yield）看成是对行首缩进的复位，那样当我们再次return/yield时，我们可以这么写代码



```f#
Bind(1,fun x -> 
   Bind(2,fun y -> 
     Bind(x + y,fun z -> 
        Yield(z)  
// start a new expression        
Bind(3,fun w -> 
   Bind(4,fun u -> 
     Bind(w + u,fun v -> 
        Yield(v)
```



然而这段代码可以被简化成

```f#
let value1 = some expression 
let value2 = some other expression
```

也就是说，我们在computation expression中有两个值，现在问题很明显，如何让这两个值结合成一个值作为整个computation expression的返回结果？

这是一个关键点。对单个computation expression，return和yield不提前返回。Computation expression的每个部分总是被计算——不会有短路。如果我们想短路并提前返回，我们必须写代码来实现。

回到刚才提出的问题。我们有两个表达式，这两个表达式有两个结果值：如何将多个值结合到一个值里面？

## 介绍"Combine"

上面问题的答案就是使用“combine”方法，这个方法输入参数为两个包装类型值，然后将这两个值结合生成另外一个包装值。

在我们的例子中，我们使用int option，故一个简单的实现就是将数字加起来。每个参数是一个option类型，需要考虑四种情况，代码如下



```f#
type TraceBuilder() =
    // other members as before

    member this.Combine (a,b) = 
        match a,b with
        | Some a', Some b' ->
            printfn "combining %A and %A" a' b' 
            Some (a' + b')
        | Some a', None ->
            printfn "combining %A with None" a' 
            Some a'
        | None, Some b' ->
            printfn "combining None with %A" b' 
            Some b'
        | None, None ->
            printfn "combining None with None"
            None

// make a new instance        
let trace = new TraceBuilder()
```



运行测试代码

```f#
trace { 
    yield 1
    yield 2
    } |> printfn "Result for yield then yield: %A" 
```

然而，这次却获得了一个不同的错误

This control construct may only be used if the computation expression builder defines a 'Delay' method

Delay方法类似一个钩子，使computation expression延迟计算，直到需要用到其值时才进行计算。一会我们将讨论这其中的细节。现在，我们创建一个默认实现



```f#
type TraceBuilder() =
    // other members as before

    member this.Delay(f) = 
        printfn "Delay"
        f()

// make a new instance        
let trace = new TraceBuilder()
```



再次运行测试代码

```f#
trace { 
    yield 1
    yield 2
    } |> printfn "Result for yield then yield: %A" 
```

最后我们获得结果如下



```f#
Delay
Yield an unwrapped 1 as an option
Delay
Yield an unwrapped 2 as an option
combining 1 and 2
Result for yield then yield: Some 3
```



 整个工作流的结果为所有yield的和，即3。

如果在工作流中发生一个“错误”（例如，None），那第二个yield不发生，总的结果为Some 1



```f#
trace { 
    yield 1
    let! x = None
    yield 2
    } |> printfn "Result for yield then None: %A" 
```



使用三个yield


```f#

trace { 
    yield 1
    yield 2
    yield 3
    } |> printfn "Result for yield x 3: %A" 
```



结果如期望，为Some 6

我们甚至可以混用yield和return。除了语法不同，结果是相同的



```f#
trace { 
    yield 1
    return 2
    } |> printfn "Result for yield then return: %A" 

trace { 
    return 1
    return 2
    } |> printfn "Result for return then return: %A" 
```



## 使用Combine实现顺序产生结果

将数值加起来不是yield真正的目的，尽管你也可以使用yield类似地将字符串连接起来，就像StringBuilder一样。

yield更一般地是用来顺序产生结果，现在我们已经知道Combine，我们可以使用Combine和Delay方法来扩展“ListBuilder”工作流

*   Combine方法是连接list
*   Delay方法使用默认的实现

整个建造类如下



```f#
type ListBuilder() =
    member this.Bind(m, f) = 
        m |> List.collect f

    member this.Zero() = 
        printfn "Zero"
        []
        
    member this.Yield(x) = 
        printfn "Yield an unwrapped %A as a list" x
        [x]

    member this.YieldFrom(m) = 
        printfn "Yield a list (%A) directly" m
        m

    member this.For(m,f) =
        printfn "For %A" m
        this.Bind(m,f)
        
    member this.Combine (a,b) = 
        printfn "combining %A and %A" a b 
        List.concat [a;b]

    member this.Delay(f) = 
        printfn "Delay"
        f()

// make an instance of the workflow                
let listbuilder = new ListBuilder()
```



下面使用它的代码


```f#

listbuilder { 
    yield 1
    yield 2
    } |> printfn "Result for yield then yield: %A" 

listbuilder { 
    yield 1
    yield! [2;3]
    } |> printfn "Result for yield then yield! : %A" 
```



以下是一个更为复杂的例子，这个例子使用了for循环和一些yield


```f#

listbuilder { 
    for i in ["red";"blue"] do
        yield i
        for j in ["hat";"tie"] do
            yield! [i + " " + j;"-"]
    } |> printfn "Result for for..in..do : %A" 
```



然后结果为

```f#
["red"; "red hat"; "-"; "red tie"; "-"; "blue"; "blue hat"; "-"; "blue tie"; "-"]
```

可以看到，结合for..in..do和yield，我们已经很接近内建的seq表达式语法了（当然，除了不像seq那样的延迟特性）。

我强烈建议你再回味一下以上那些内容，直到非常清楚在那些语法的背后发生了什么。正如你在上面的例子中看到的一样，你创造性地可以使用yeild产生各种不规则list，而不仅仅是简单的list

说明：如果想知道while，我们将延后一些，直到我们在下一篇中讲完了Delay之后再来讨论while。

## "Combine"处理顺序

Combine方法只有两个输入参数，那如果组合多个两个的值呢？例如，下面代码组合4个值


```f#

listbuilder { 
    yield 1
    yield 2
    yield 3
    yield 4
    } |> printfn "Result for yield x 4: %A" 
```



如果你看输出，你将会知道是成对地组合值

```f#
combining [3] and [4]
combining [2] and [3; 4]
combining [1] and [2; 3; 4]
Result for yield x 4: [1; 2; 3; 4]
```

更准确地说，它们是从最后一个值开始，向后被组合起来。“3”和“4”组合，结果再与“2”组合，如此类推。

![图片](http://fsharpforfunandprofit.com/assets/img/combine.png)

## 无序的Combine

在之前的第二个有问题的例子中，表达式是无序的，我们只是让两个独立的表达式处于同一行中

```f#
trace { 
    if true then printfn "hello"  //expression 1
    return 1                      //expression 2
    } |> printfn "Result for combine: %A"
```

此时，如何组合组合表达式？

有很多通用的方法，具体是哪种方法还依赖于工作流想实现什么目的。

### 为有“success”或“failure”的工作流实现combine


 如果工作流有“success”或者“failure”的概念，则一个标准的方法是：

*   如果第一个表达式“succeeds”（执行成功），则使用表达式的值
*   否则，使用第二个表达式的值

在本例中，我们通常对Zero使用“failure”值。

在将一系列的“or else”表达式链接起来时，这个方法非常有用，第一个成功的表达式的值将成为整体的返回值。

```f#
if (do first expression)
or else (do second expression)
or else (do third expression)
```

例如对maybe工作流，如果第一个表达式结果是Some，则返回第一个表达式的值，否则返回第二个表达式的值，如下所示


```f#

type TraceBuilder() =
    // other members as before
    
    member this.Zero() = 
        printfn "Zero"
        None  // failure
    
    member this.Combine (a,b) = 
        printfn "Combining %A with %A" a b
        match a with
        | Some _ -> a  // a succeeds -- use it
        | None -> b    // a fails -- use b instead
        
// make a new instance        
let trace = new TraceBuilder()
```



 **例子：解析** 

试试一个有解析功能的例子，其实现如下



```f#
type IntOrBool = I of int | B of bool

let parseInt s = 
    match System.Int32.TryParse(s) with
    | true,i -> Some (I i)
    | false,_ -> None

let parseBool s = 
    match System.Boolean.TryParse(s) with
    | true,i -> Some (B i)
    | false,_ -> None

trace { 
    return! parseBool "42"  // fails
    return! parseInt "42"
    } |> printfn "Result for parsing: %A"
```



结果如下

Some (I 42)

可以看到第一个return！表达式结果为None，它被忽略掉，所以整个表达式结果为第二个表达式的值，Some (I 42)

 **例子：查字典** 

在这个例子中，我们在一些字典中查询一些键，并在找到对应的值的时候返回


```f#

let map1 = [ ("1","One"); ("2","Two") ] |> Map.ofList
let map2 = [ ("A","Alice"); ("B","Bob") ] |> Map.ofList

trace { 
    return! map1.TryFind "A"
    return! map2.TryFind "A"
    } |> printfn "Result for map lookup: %A" 
```



结果如下

Result for map lookup: Some "Alice"

可以看到，第一个查询结果为None，它被忽略掉，故整个语句结果为第二次查询结果值

从上面的讨论可见，这个技术在解析或者计算一系列操作（可能不成功）时非常方便。

### 为带有顺序步骤的工作流实现“combine”


如果工作流的操作步骤是顺序的，那整体的结果就是最后一步的值，而前面步骤的计算仅是为了获得边界效应（副作用，如改变某些变量的值）。

通常在F#中，顺序步骤可能会写成这样

```f#
do some expression
do some other expression 
final expression

```
或者使用分号语法，即

```f#
some expression; some other expression; final expression
```

在普通的F#语句中，最后一个表达式除外的每个表达式的计算结果值均为unit。

Computation expression的等效顺序操作是将每个表达式（最后一个表达式除外）看成一个unit的包装类型值，然后将这个值传入下一个表达式，如此类推，直到最后一个表达式。

这就跟bind所做的事情差不多，所以最简单的实现就是再次利用Bind方法。当然，这里Zero就是unit的包装值


```f#

type TraceBuilder() =
    // other members as before

    member this.Zero() = 
        printfn "Zero"
        this.Return ()  // unit not None

    member this.Combine (a,b) = 
        printfn "Combining %A with %A" a b
        this.Bind( a, fun ()-> b )
        
// make a new instance        
let trace = new TraceBuilder()
```



与普通的bind不同的是，这个continuation有一个unit类型的输入，然后计算b。这反过来要求a是WrapperType<unit>类型，或者更具体地，如我们这里例子中的unit option

以下是一个顺序过程的例子，实现了Combine


```f#

trace { 
    if true then printfn "hello......."
    if false then printfn ".......world"
    return 1
    } |> printfn "Result for sequential combine: %A" 

```


输出结果为


```f#

hello.......
Zero
Returning a unwrapped <null> as an option
Zero
Returning a unwrapped <null> as an option
Returning a unwrapped 1 as an option
Combining Some null with Some 1
Combining Some null with Some 1
Result for sequential combine: Some 1
```



注意整个语句的结果是最后一个表达式的值。

### 为创建数据结构的工作流实现“combine”


最后，还有一个工作流的常见模式是创建数据结构。在这种情况下，Combine应该合并两个数据结构，并且如果需要的话（如果可能），Zero方法应该创建一个空数据结构。

在前面的“list builder”例子中，我们使用的就是这个方法。Combine结合两个列表，并且Zero是空列表。

## 混合“Combine”与“Zero”的说明

我们已经看到关于option类型的两种不同的Combine实现。

*   第一个使用options指示“success/failure”，第一个成功的表达式结果即为最终的结果值，在这个情况下，Zero被定义成None。
*   第二个是顺序步骤操作的例子，在这种情况下，Zero被定义成Some ()

两种情况均能良好的工作，但是这两个例子是否只是侥幸能正常工作？有没有关于正确实现Combine和Zero的指导说明？

首先，如果输入参数交换位置，Combine不必返回相同的结果值，即，Combine(a,b)和Combine(b,a)不需要相同。“list builder”就是一个很好的例子

另外，把Zero与Combine连接起来是很有用的。

 **规则：Combine(a,Zero)应该与Combine(Zero,a)相同，而Combine(Zero,a)应该与a相同。** 

为了使用算法的类比，你可以把Combine看成加法（这不是一个差劲的类比——它确实将两个值相加）。当然，Zero就是数字0，故上面的这条规则可以表述成：

 **规则：a+0与0+a相同，与a相同，而+表示Combine，0表示Zero。** 

如果你观察有关option类型的第一个Combine实现（“success/failure”），你会发现它确实与这条规则符合，第二个实现（“bind” with Some()）也是如此。

另外一方面，如果我们已经使用“bind”来实现Combine，将Zero定义成None，则它不遵循这个规则，这意味着我们已经碰到一些错误。

## 不带bind的“Combine”

关于其他的builder方法，如果不需要它们，则不必实现这些方法。故对一个严格顺序的工作流而言，可以简单地创建一个包含Combine、Zero和Yield方法的建造类（builder class），也就是，不用实现Bind和Return。

以下是一个最简单的实现



```f#
type TraceBuilder() =

    member this.ReturnFrom(x) = x

    member this.Zero() = Some ()

    member this.Combine (a,b) = 
        a |> Option.bind (fun ()-> b )

    member this.Delay(f) = f()

// make an instance of the workflow                
let trace = new TraceBuilder()
```



使用方法如下



```f#
trace { 
    if true then printfn "hello......."
    if false then printfn ".......world"
    return! Some 1
    } |> printfn "Result for minimal combine: %A" 

```


类似地，如果你有一个面向数据结构的工作流，可以只实现Combine和其他一些帮助方法。例如，以下为一个list builder类的简单实现


```f#

type ListBuilder() =

    member this.Yield(x) = [x]

    member this.For(m,f) =
        m |> List.collect f

    member this.Combine (a,b) = 
        List.concat [a;b]

    member this.Delay(f) = f()

// make an instance of the workflow                
let listbuilder = new ListBuilder()
```



尽管这是最简单的实现，我们依然可以如下写使用代码



```f#
listbuilder { 
    yield 1
    yield 2
    } |> printfn "Result: %A" 

listbuilder { 
    for i in [1..5] do yield i + 2
    yield 42
    } |> printfn "Result: %A"
```



## 独立的Combine函数

在上一篇中，我们看到“bind”函数通常被当成一个独立函数来使用，并用操作符 >>= 来表示。

Combine函数亦是如此，常被当成一个独立函数来使用。跟bind不同的是，Combine没有一个标准符号——它可以变化，取决于combine函数的用途。

一个符号化的combination操作通常写成 ++ 或者 <+>。我们之前对options使用的“左倾”的combination（即，如果第一个表达式失败，则只执行第二个表达式）有时候写成 <++。

以下是一个关于options的独立的左倾combination，跟上面那个查询字典的例子类似。



```f#
module StandaloneCombine = 

    let combine a b = 
        match a with
        | Some _ -> a  // a succeeds -- use it
        | None -> b    // a fails -- use b instead

    // create an infix version
    let ( <++ ) = combine

    let map1 = [ ("1","One"); ("2","Two") ] |> Map.ofList
    let map2 = [ ("A","Alice"); ("B","Bob") ] |> Map.ofList

    let result = 
        (map1.TryFind "A") 
        <++ (map1.TryFind "B")
        <++ (map2.TryFind "A")
        <++ (map2.TryFind "B")
        |> printfn "Result of adding options is: %A"

```


## 总结

这篇文章中我们学到Combine的哪些内容？

*   如果在一个computation expression中需要combine或者“add”不止一个的包装类型值，则需要实现Combine（和Delay）
*   Combine方法从后往前地将值成对地结合起来
*   没有一个通用的Combine实现能处理所有情况——需要根据工作流具体的需要定义不同的Combine实现
*   有将Combine关系到Zero的敏感规则
*   Combine不依赖Bind的实现
*   Combine可以被当成一个独立的函数暴露出来

下一篇中，当计算内部的表达式时，我们增加一些逻辑控制，并引入正确的短路和延迟计算。

